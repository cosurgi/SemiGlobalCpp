/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#include "MultiVector.hpp"

// MultiVectorXcr , MultiVectorXcr
MultiVectorXcr operator+(const MultiVectorXcr& a, const MultiVectorXcr& b)
{
	assert(a.size() == b.size());
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() + b[i].array();
	}
	return ret;
}

MultiVectorXcr operator-(const MultiVectorXcr& a, const MultiVectorXcr& b)
{
	assert(a.size() == b.size());
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() - b[i].array();
	}
	return ret;
}

MultiVectorXcr operator*(const MultiVectorXcr& a, const MultiVectorXcr& b)
{
	assert(a.size() == b.size());
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() * b[i].array();
	}
	return ret;
}

MultiVectorXcr operator/(const MultiVectorXcr& a, const MultiVectorXcr& b)
{
	assert(a.size() == b.size());
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() / b[i].array();
	}
	return ret;
}

// Multiply each element of a by b.
MultiVectorXcr operator*(const MultiVectorXcr& a, const VectorXcr& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() * b.array();
	}
	return ret;
}

// Complex , MultiVectorXcr
MultiVectorXcr operator+(const Complex& a, const MultiVectorXcr& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(b);
	for (size_t i = 0; i < b.size(); i++) {
		ret[i] = a + b[i].array();
	}
	return ret;
}

MultiVectorXcr operator-(const Complex& a, const MultiVectorXcr& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(b);
	for (size_t i = 0; i < b.size(); i++) {
		ret[i] = a - b[i].array();
	}
	return ret;
}

MultiVectorXcr operator*(const Complex& a, const MultiVectorXcr& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(b);
	for (size_t i = 0; i < b.size(); i++) {
		ret[i] = a * b[i].array();
	}
	return ret;
}

MultiVectorXcr operator/(const Complex& a, const MultiVectorXcr& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(b);
	for (size_t i = 0; i < b.size(); i++) {
		ret[i] = a / b[i].array();
	}
	return ret;
}

// MultiVectorXcr , Complex
MultiVectorXcr operator+(const MultiVectorXcr& a, const Complex& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() + b;
	}
	return ret;
}

MultiVectorXcr operator-(const MultiVectorXcr& a, const Complex& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() - b;
	}
	return ret;
}

MultiVectorXcr operator*(const MultiVectorXcr& a, const Complex& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() * b;
	}
	return ret;
}

MultiVectorXcr operator/(const MultiVectorXcr& a, const Complex& b)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(a);
	for (size_t i = 0; i < a.size(); i++) {
		ret[i] = a[i].array() / b;
	}
	return ret;
}

// creates zeroed MultiVectorXcr with the size same as arg a
MultiVectorXcr getZeroMultiVectorXcr(const MultiVectorXcr& a)
{
	assert(a.size() > 0);
	MultiVectorXcr ret(a.size(), VectorXcr::Zero(a[0].size()));
	return ret;
}

// convert between MultiVectorXcr ↔ VectorXcr
VectorXcr flatten(const MultiVectorXcr& a)
{
	assert(a.size() > 0);
	auto      topSize = a.size();
	auto      inSize  = a[0].size();
	VectorXcr ret(topSize * inSize);
	for (size_t i = 0; i < topSize; i++) {
		ret.segment(i * inSize, inSize) = a[i];
	}
	return ret;
}

void decompres(const VectorXcr in, MultiVectorXcr& out /* must have proper size */)
{
	assert(out.size() > 0);
	auto topSize = out.size();
	auto inSize  = out[0].size();
	assert(in.size() == static_cast<long int>(topSize) * inSize);
	for (size_t i = 0; i < topSize; i++) {
		out[i] = in.segment(i * inSize, inSize);
	}
}

// IO
std::ostream& operator<<(std::ostream& os, const MultiVectorXcr& a)
{
	os << "size = " << a.size() << "\n";
	for (const auto& v : a) {
		os << v << "\n---\n";
	}
	return os;
}
