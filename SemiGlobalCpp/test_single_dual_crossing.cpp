/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#include "Constants.hpp"
#include "FFT.hpp"
#include "MultiVector.hpp"
#include "SemiGlobal.hpp"
#include "Tully1990_Coker1993_Helpers.hpp"
#include <boost/core/demangle.hpp>
#include <iostream>

// returns 'x' coordinate of a grid point i in a table
inline Real iToX(Real i /* grid point index i */, const Real& points, const Real& length) { return (i * length) / points; };

// Gaussian wavepacket
Complex gaussWavePacket(
        Real x,   // position where wavepacket is calculated
        Real x0,  // initial position of wavepacket center at time t=t0
        Real t,   // time when wavepacket is evaluated
        Real t0,  // initial time
        Real k0,  // initial wavenumber of wavepacket
        Real m,   // particle mass
        Real a,   // wavepacket width, sometimes called sigma, of the Gaussian distribution
        Real hbar // Planck's constant divided by 2pi
)
{
	if (a <= 0) {
		std::cerr << __PRETTY_FUNCTION__ << " : bad arg a=" << a << "\n";
		exit(1);
	}
	x -= x0;
	t -= t0;
	// clang-format off
	return exp(
		-(
			(m*x*x+Mathr::I*a*a*k0*(k0*hbar*t-Real(2)*m*x))
			/
			(Real(2)*a*a*m+Real(2)*Mathr::I*hbar*t)
		)
	)
	/
	(
		pow(Mathr::PI,Real(0.25))*(pow(a+Mathr::I*hbar*t/(a*m),Complex(0.5)))
	);
	// clang-format on
};

// helper function to print the wavefunction
void printVec(const VectorXcr& vec, const Real& points, const Real& length)
{
	std::cout << std::setprecision(19);
	std::cout << "R[a0]            wavefunction real, imag parts\n";
	for (int i = 0; i < vec.size(); i++) {
		std::cout << std::setw(20) << std::left << iToX(i, points, length) << " " << std::setw(25) << vec(i).real() << " " << std::setw(25)
		          << vec(i).imag() << "\n";
	}
}

// struct holding the wavefunction state
struct State {
	int                                 levels;
	size_t                              points;
	Real                                mass;
	Real                                length;
	MultiVectorXcr                      wf; // wavefunction
	VectorXcr                           minusKSqared;
	std::vector<std::vector<VectorXcr>> potentialMatrix;

	State(int levels_, int points_, Real mass_, Real length_)
	        : levels(levels_)
	        , points(points_)
	        , mass(mass_)
	        , length(length_)
	        , wf(levels, VectorXcr::Zero(points_))
	        , minusKSqared(points_)
	{
		calcMinusKSquared();
	};

	Real           deltaX() const;
	Real           lambdaMin() const;
	Real           kMax() const;
	Real           iToK(Real i /* this is the index i in the grid */) const;
	void           calcMinusKSquared();
	VectorXcr      calc_Ekin_psi_single(const VectorXcr& psi_0);
	MultiVectorXcr calc_Hpsi(const MultiVectorXcr& psi_0, const Real& t);
	VectorXcr      minus_i_Hpsi__MultiVectorFlattened(const VectorXcr& u, const Real& t, const VectorXcr& v);
	bool           hasTimeDependence(int j, int k) const;
	VectorXcr      timeDependentPotential(int j, int k, Real t) const;
};

Real State::deltaX() const { return length / points; };
Real State::lambdaMin() const { return 2 * deltaX(); };
Real State::kMax() const { return Mathr::TWO_PI / lambdaMin(); };

Real State::iToK(Real n /* this is the index i in the grid */) const
{
	auto N = points;
	return kMax() * ((2 * n / N) - 1);
};

void State::calcMinusKSquared()
{
	using std::pow;
	auto N = points;
	for (int i = 0; i < minusKSqared.size(); i++) {
		minusKSqared(i) = -pow(iToK((i + N / 2) % N), 2);
	}
};

bool State::hasTimeDependence(int /* j */, int /* k */) const
{
	// if Hamiltonian[j][k] has time dependency then this function returns true
	return false; // in this example we have no time dependent potential
}

VectorXcr State::timeDependentPotential(int /* j */, int /* k */, Real /* t */) const
{
	// if Hamiltonian[j][k] has time dependency then this function returns potentialMatrix[j][k] at time 't'
	// This can be for example the electric field of the laser added to constant potentialMatrix.
	assert(false); // in this example we have no time dependent potential
}

VectorXcr State::calc_Ekin_psi_single(const VectorXcr& psi_0)
{
	VectorXcr psi_1 = VectorXcr::Zero(points);
	doFFT(psi_0, psi_1);                            // ψ₁=                  ℱ(ψ₀)
	psi_1 = (psi_1.array() * minusKSqared.array()); // ψ₁=               -k²ℱ(ψ₀)
	doIFFT(psi_1, psi_1);                           // ψ₁=           ℱ⁻¹(-k²ℱ(ψ₀)) = ∇²ψ₀
	psi_1 *= (-PhysConst::hbarSqr / (2 * mass));    // ψ₁=    (-ℏ²(∇²ψ₀)/(2m))
	return psi_1;
}

MultiVectorXcr State::calc_Hpsi(const MultiVectorXcr& psi_0, const Real& t)
{
	MultiVectorXcr psi_ret = getZeroMultiVectorXcr(psi_0);

	// The kinetic operator, acting only on the diagonal
	for (int j = 0; j < levels; j++) {
		psi_ret[j] = calc_Ekin_psi_single(psi_0[j]);
	}

	// The potential operator matrix acting on the wavefunction
	for (int j = 0; j < levels; j++) {
		for (int k = 0; k < levels; k++) {
			// obtain current value of time dependent potential only if it's present.
			if (hasTimeDependence(j, k)) {
				psi_ret[j] = psi_ret[j].array() + timeDependentPotential(j, k, t).array() * psi_0[k].array();
			} else {
				psi_ret[j] = psi_ret[j].array() + potentialMatrix[j][k].array() * psi_0[k].array();
			}
		}
	}

	return psi_ret;
}

VectorXcr State::minus_i_Hpsi__MultiVectorFlattened(
        const VectorXcr& /* u */, // wavefunction from the "future" timestep, it is used in e.g. Bose-Einstein Condensate trap
        const Real&      t,       // time
        const VectorXcr& v        // wavefunction from previous timestep - this is the input into the Hamiltonian
)
{
	MultiVectorXcr psi_work = getZeroMultiVectorXcr(wf);
	decompres(v, psi_work);
	psi_work = calc_Hpsi(psi_work, t);
	return -Mathr::I * flatten(psi_work);
};

// This function prints the data used to reproduce Figures 11, 13, 15 and 16 in article by Coker 1993
// They are the main indicator that the SemiGlobal algorithm works correctly for multiple electronic levels.
void printTransmissionReflection(
        std::ostream& out, Real t, Real startX, Real length, int points, const MultiVectorXcr& wf, const std::vector<MatrixXr>& eqn65matrixP)
{
	auto trans         = -startX;
	auto fragment      = trans / length;
	auto barrier       = int(points * fragment);
	auto rest          = points - barrier;
	auto phi           = rotateWaveFunctionEqn69(wf, eqn65matrixP);
	auto deltaX        = length / points;
	auto reflection0   = (phi[0].segment(0, barrier).dot(phi[0].segment(0, barrier)) * deltaX).real();
	auto reflection1   = (phi[1].segment(0, barrier).dot(phi[1].segment(0, barrier)) * deltaX).real();
	auto transmission0 = (phi[0].segment(barrier, rest).dot(phi[0].segment(barrier, rest)) * deltaX).real();
	auto transmission1 = (phi[1].segment(barrier, rest).dot(phi[1].segment(barrier, rest)) * deltaX).real();
	out << t << " " << reflection0 << " " << reflection1 << " " << transmission0 << " " << transmission1 << "\n";
}

int main(int argc, char* argv[])
{
	// Grid parameters
	size_t points { 0 };  // number of grid points, set below depending on simulation type
	Real   length { 0 };  // size in atomic units a_0, also set below
	Real   startX { 0 };  // the X coordinate of the first point in a grid
	Real   maxTime { 0 }; // how long time to propagate
	// Model parameters
	Real x0 { 0 };               // starting position of the wavepacket, also set below
	Real k0 { 0 };               // wavepacket momentum, set below
	Real a0 { 1 };               // wavepacket width, set below
	Real m { 2000 };             // mass is 2000 a.u.
	Real hbar = PhysConst::hbar; // actually 1
	int  crossing { 0 };         // '1' or '2' - how many potential crossings

	std::cerr << "Using precision " << boost::core::demangle(typeid(Real).name()) << "\n";
	int paramSet = 0;
	if (argc == 2) {
		paramSet = boost::lexical_cast<int>(argv[1]);
		switch (paramSet) {
			case 1:
				std::cerr << "Single crossing, high momentum\n";
				points   = 1024;
				length   = 20;
				startX   = -10;
				maxTime  = 1200;
				x0       = -4 - startX; // 6
				k0       = 15;          // Coker1993 Fig_11
				a0       = 0.75;
				crossing = 1;
				break;
			case 2:
				std::cerr << "Single crossing, low momentum\n";
				points   = 2048; // raczej nie wpływa jak dam 4096
				length   = 40;
				startX   = -10;
				maxTime  = 4000;
				x0       = -4.15 - startX; // 5.85
				k0       = 8.5;            // Coker1993 Fig_13
				a0       = 0.80;
				crossing = 1;
				break;
			case 3:
				std::cerr << "Dual crossing, high momentum\n";
				points   = 2048;
				length   = 34;
				startX   = -12;
				maxTime  = 900;
				x0       = -8 - startX; // 4;
				k0       = 52;          // Coker1993 Fig_16
				a0       = 0.70;
				crossing = 2;
				break;
			case 4:
				std::cerr << "Dual crossing, low momentum\n";
				points   = 2048;
				length   = 34;
				startX   = -12;
				maxTime  = 1500;
				x0       = -8 - startX; // 4;
				k0       = 30;          // Coker1993 Fig_15
				a0       = 0.70;
				crossing = 2;
				break;
			default: std::cerr << "Please select parameter set: 1,2,3 or 4\n"; exit(1);
		}
	} else {
		std::cerr << "One argument needed: 1|2|3|4 to select the parameter set.\n";
		exit(1);
	}

	// Create the wavefunction
	State state(/* two levels */ 2, points, /* mass */ m, length);
	for (int i = 0; i < state.wf[0].size(); i++) {
		state.wf[0](i) = gaussWavePacket(iToX(i, points, length), x0, 0, 0, k0, m, a0, hbar);
	}
	// Uncomment to print the initial wavefunction
	//printVec(state.wf[0], points, length);
	//printVec(state.wf[1], points, length);

	// Now create the potential
	state.potentialMatrix = getTullyNonAdiabatic(points, length, startX, crossing);

	// Uncomment to print the potential
	//printVec(potentialMatrix[0][0], points, length);
	//printVec(potentialMatrix[0][1], points, length);
	//printVec(potentialMatrix[1][0], points, length);
	//printVec(potentialMatrix[1][1], points, length);

	std::vector<MatrixXr> eqn65matrixP; // equation 65, in Coker1993
	eqn65matrixP = getTullyAdiabatic_ObtainedFromEqn66_1993Coker(points, length, state.potentialMatrix).second;
	// Uncomment to print the matrix P, which is later used to convert between diabatic ↔ adiabatic
	//for(const auto& a : eqn65matrixP) {
	//	std::cout << a << "\n\n";
	//}

	// Prepare the SemiGlobalODE
	Real dt    = 1;     // timestep
	Real tol   = 1e-10; // error tolerance, when using float128 consider using here lower tolerance like 2e-34
	int  Nt_ts = 3;     // The number of interior Chebyshev time points in each time-step, used during the computational process (M in the article).
	int  Nfm   = 8;     // The number of expansion terms for the computation of the function of matrix (K in the article).
	using namespace std::placeholders;
	GOperatorFunction Gop     = std::bind(&State::minus_i_Hpsi__MultiVectorFlattened, &state, _1, _2, _3); // -i*H(t)ψ
	auto              startWf = flatten(state.wf);
	SemiGlobalODE     ode(Gop, OptionalGDiffFunc(boost::none), OptionalInhFunc(boost::none), OptVecXcr(boost::none), startWf, dt, Nt_ts, Nfm, tol, 0, 7);

	// start propagation
	for (int iteration = 0; iteration * dt < maxTime; iteration++) {
		ode.propagateByDtSemiGlobal(iteration);
		// get the current wavefunction
		decompres(ode.getFiSolution(), state.wf);
		printTransmissionReflection(std::cout, (iteration + 1) * dt, startX, length, points, state.wf, eqn65matrixP);
	}
}
