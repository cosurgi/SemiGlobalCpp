/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

//#define USE_EIGEN_FFT // not working, fft.inv(…) gives bad result because it's in-place.
#define FFTW_JEDEN_PLAN_SZYBSZE // fftw single plan - it's faster
#include "Real.hpp"

#ifndef USE_EIGEN_FFT
#include <fftw3.h> // not thread-safe
#else
#include <unsupported/Eigen/FFT>
#endif

#include <type_traits>

#ifdef USE_EIGEN_FFT // not working because it's "in-place"
template <typename C> void doFFT(const Eigen::Matrix<C, Eigen::Dynamic, 1>& in, Eigen::Matrix<C, Eigen::Dynamic, 1>& out)
{
	auto                                                         N = in.size();
	/* static thread_local */ Eigen::FFT<typename C::value_type> fft;
	//static thread_local Eigen::FFT<C> fft;
	//fft.fwd(in.col(0).data(), out.col(0).data(), N);
	//fft.fwd(in.col(0).data(), out.col(0).data(), N);
	fft.fwd(out.data(), in.data(), N);
}

template <typename C> void doIFFT(const Eigen::Matrix<C, Eigen::Dynamic, 1>& in, Eigen::Matrix<C, Eigen::Dynamic, 1>& out)
{
	//std::cerr << in << "\n\n";
	//exit(0);
	auto                                                         N = in.size();
	/* static thread_local */ Eigen::FFT<typename C::value_type> fft;
	//static thread_local Eigen::FFT<C> fft;
	//fft.inv(in.col(0), out.col(0), N);
	fft.inv(out.data(), in.data(), N); // not working because it's in-place in.data() == out.data()
	//out = out.array() / N;

	std::cerr << out << "\n\n";
	exit(0);
}

#else
// XXX: libfftw is not thread-safe

template <typename C> void doFFT(const Eigen::Matrix<C, Eigen::Dynamic, 1>& in, Eigen::Matrix<C, Eigen::Dynamic, 1>& out);
template <typename C> void doIFFT(const Eigen::Matrix<C, Eigen::Dynamic, 1>& in, Eigen::Matrix<C, Eigen::Dynamic, 1>& out);
/* TODO: a 2d FFT:
template <typename C> void doDCT_2d(const Eigen::Matrix<C, Eigen::Dynamic, Eigen::Dynamic>& in, Eigen::Matrix<C, Eigen::Dynamic, Eigen::Dynamic>& out);
*/
#ifdef FFTW_JEDEN_PLAN_SZYBSZE // fftw single plan - it's faster
// it's faster but I have to be careful so that:
// 1. data alignment is correct
// 2. (in.data() == out.data()) or (in.data() != out.data()), has to stay the same in subsequent calls
template <> void doFFT(const Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	assert(in.data() != out.data()); // because doFFT is called not "in-place"
	// see http://www.fftw.org/doc/New_002darray-Execute-Functions.html#New_002darray-Execute-Functions
	static thread_local int ali_in  = fftw_alignment_of((((double(*)[2])(in.data()))[0]));
	static thread_local int ali_out = fftw_alignment_of((((double(*)[2])(out.data()))[0]));
	assert(ali_in == fftw_alignment_of((((double(*)[2])(in.data()))[0])));
	assert(ali_out == fftw_alignment_of((((double(*)[2])(out.data()))[0])));
	auto                          N = in.size();
	static thread_local fftw_plan p = fftw_plan_dft_1d(N, (double(*)[2])(in.data()), (double(*)[2])(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute_dft(p, (double(*)[2])(in.data()), (double(*)[2])(out.data()));
}

template <> void doFFT(const Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	assert(in.data() != out.data());
	auto                           N = in.size();
	static thread_local fftwl_plan p = fftwl_plan_dft_1d(N, (long double(*)[2])(in.data()), (long double(*)[2])(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
	fftwl_execute_dft(p, (long double(*)[2])(in.data()), (long double(*)[2])(out.data()));
}

template <>
void doFFT(const Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	assert(in.data() != out.data());
	auto                           N = in.size();
	static thread_local fftwq_plan p
	        = fftwq_plan_dft_1d(N, (boost::float128_t(*)[2])(in.data()), (boost::float128_t(*)[2])(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
	fftwq_execute_dft(p, (boost::float128_t(*)[2])(in.data()), (boost::float128_t(*)[2])(out.data()));
}


template <> void doIFFT(const Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	assert(in.data() == out.data()); // however doIFFT is called "in-place"
	// see http://www.fftw.org/doc/New_002darray-Execute-Functions.html#New_002darray-Execute-Functions
	static thread_local int ali_in  = fftw_alignment_of((((double(*)[2])(in.data()))[0]));
	static thread_local int ali_out = fftw_alignment_of((((double(*)[2])(out.data()))[0]));
	assert(ali_in == fftw_alignment_of((((double(*)[2])(in.data()))[0])));
	assert(ali_out == fftw_alignment_of((((double(*)[2])(out.data()))[0])));
	auto                          N = in.size();
	static thread_local fftw_plan p = fftw_plan_dft_1d(N, (double(*)[2])(in.data()), (double(*)[2])(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute_dft(p, (double(*)[2])(in.data()), (double(*)[2])(out.data()));
	out = out.array() / N;
}

template <> void doIFFT(const Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	assert(in.data() == out.data());
	auto                           N = in.size();
	static thread_local fftwl_plan p = fftwl_plan_dft_1d(N, (long double(*)[2])(in.data()), (long double(*)[2])(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
	fftwl_execute_dft(p, (long double(*)[2])(in.data()), (long double(*)[2])(out.data()));
	out = out.array() / N;
}

template <>
void doIFFT(const Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	assert(in.data() == out.data());
	auto                           N = in.size();
	static thread_local fftwq_plan p
	        = fftwq_plan_dft_1d(N, (boost::float128_t(*)[2])(in.data()), (boost::float128_t(*)[2])(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
	fftwq_execute_dft(p, (boost::float128_t(*)[2])(in.data()), (boost::float128_t(*)[2])(out.data()));
	out = out.array() / N;
}

#else // previous version of the code
template <> void doFFT(const Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	auto      N = in.size();
	fftw_plan p;
	p = fftw_plan_dft_1d(N, (double(*)[2])(in.data()), (double(*)[2])(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);
}

template <> void doFFT(const Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	auto       N = in.size();
	fftwl_plan p;
	p = fftwl_plan_dft_1d(N, (long double(*)[2])(in.data()), (long double(*)[2])(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
	fftwl_execute(p);
	fftwl_destroy_plan(p);
}

template <>
void doFFT(const Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	auto       N = in.size();
	fftwq_plan p;
	p = fftwq_plan_dft_1d(N, (boost::float128_t(*)[2])(in.data()), (boost::float128_t(*)[2])(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
	fftwq_execute(p);
	fftwq_destroy_plan(p);
}


template <> void doIFFT(const Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	auto      N = in.size();
	fftw_plan p;
	p = fftw_plan_dft_1d(N, (double(*)[2])(in.data()), (double(*)[2])(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);
	out = out.array() / N;
}

template <> void doIFFT(const Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<long double>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	auto       N = in.size();
	fftwl_plan p;
	p = fftwl_plan_dft_1d(N, (long double(*)[2])(in.data()), (long double(*)[2])(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
	fftwl_execute(p);
	fftwl_destroy_plan(p);
	out = out.array() / N;
}

template <>
void doIFFT(const Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& in, Eigen::Matrix<std::complex<boost::float128_t>, Eigen::Dynamic, 1>& out)
{
	assert(in.size() == out.size());
	auto       N = in.size();
	fftwq_plan p;
	p = fftwq_plan_dft_1d(N, (boost::float128_t(*)[2])(in.data()), (boost::float128_t(*)[2])(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
	fftwq_execute(p);
	fftwq_destroy_plan(p);
	out = out.array() / N;
}
#endif
#endif

/*
// discrete cosine transform 2D
template <>
void doDCT_2d(
        const Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic>& in, Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic>& out)
{
	assert(in.size() == out.size());
	auto N = in.rows();
	auto M = in.cols();
	for (int m = 0; m < M; m++) {
		fftw_plan p;
		p = fftw_plan_r2r_1d(N, (double(*)[2])(in.col(m).data()), (double(*)[2])(out.col(m).data()), FFTW_REDFT10, FFTW_ESTIMATE);
		fftw_execute(p);
		fftw_destroy_plan(p);
	}
}
*/
