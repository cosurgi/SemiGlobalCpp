# gnuplot

set xlabel "Time [a.u.]"
set ylabel "Population [-]"

set title "Single avoided crossing, high momentum k=15 a.u.,\nFig.6a in article and Fig.11 in Coker 1993 (dotted lines)"
plot './testResults/fig_11_double.txt' u 1:4 w l title 'Transmission 1' ,\
     ''                                u 1:5 w l title 'Transmission 2' ,\
     ''                                u 1:2 w l title 'Reflection 1'   ,\
     ''                                u 1:3 w l title 'Reflection 2'

pause -1

set title "Single avoided crossing, low momentum k=8.5 a.u.,\nFig.6b in article and Fig.13 in Coker 1993 (dotted lines)"
plot './testResults/fig_13_double.txt' u 1:4 w l title 'Transmission 1' ,\
     ''                                u 1:5 w l title 'Transmission 2' ,\
     ''                                u 1:2 w l title 'Reflection 1'   ,\
     ''                                u 1:3 w l title 'Reflection 2'

pause -1

set title "Dual avoided crossing, high momentum k=52 a.u.,\nFig.9a in article and Fig.16 in Coker 1993 (dotted lines)"
plot './testResults/fig_16_double.txt' u 1:4 w l title 'Transmission 1' ,\
     ''                                u 1:5 w l title 'Transmission 2' ,\
     ''                                u 1:2 w l title 'Reflection 1'   ,\
     ''                                u 1:3 w l title 'Reflection 2'

pause -1

set title "Dual avoided crossing, low momentum k=30 a.u.,\nFig.9b in article and Fig.15 in Coker 1993 (dotted lines)"
plot './testResults/fig_15_double.txt' u 1:4 w l title 'Transmission 1' ,\
     ''                                u 1:5 w l title 'Transmission 2' ,\
     ''                                u 1:2 w l title 'Reflection 1'   ,\
     ''                                u 1:3 w l title 'Reflection 2'

pause -1


