/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/
#pragma once
#include "Real.hpp"
#include <iostream>
#include <vector>

using MultiVectorXcr = std::vector<VectorXcr>;

MultiVectorXcr operator+(const MultiVectorXcr& a, const MultiVectorXcr& b);
MultiVectorXcr operator-(const MultiVectorXcr& a, const MultiVectorXcr& b);
MultiVectorXcr operator*(const MultiVectorXcr& a, const MultiVectorXcr& b);
MultiVectorXcr operator/(const MultiVectorXcr& a, const MultiVectorXcr& b);

// Multiply each element of a by b.
MultiVectorXcr operator*(const MultiVectorXcr& a, const VectorXcr& b);

MultiVectorXcr operator+(const Complex& a, const MultiVectorXcr& b);
MultiVectorXcr operator-(const Complex& a, const MultiVectorXcr& b);
MultiVectorXcr operator*(const Complex& a, const MultiVectorXcr& b);
MultiVectorXcr operator/(const Complex& a, const MultiVectorXcr& b);

MultiVectorXcr operator+(const MultiVectorXcr& a, const Complex& b);
MultiVectorXcr operator-(const MultiVectorXcr& a, const Complex& b);
MultiVectorXcr operator*(const MultiVectorXcr& a, const Complex& b);
MultiVectorXcr operator/(const MultiVectorXcr& a, const Complex& b);

// creates zeroed MultiVectorXcr with the size same as arg a
MultiVectorXcr getZeroMultiVectorXcr(const MultiVectorXcr& a);
// convert between MultiVectorXcr ↔ VectorXcr
VectorXcr flatten(const MultiVectorXcr& a);
void      decompres(const VectorXcr in, MultiVectorXcr& out /* must have proper size */);

std::ostream& operator<<(std::ostream& os, const MultiVectorXcr& a);
