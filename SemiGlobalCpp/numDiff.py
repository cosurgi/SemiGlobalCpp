#!/usr/bin/python3
# encoding: utf-8
# syntax:python

# This simple script is used by the unit tests in makefile to compare current calculations with the reference results.

import sys#, mpmath
#mpmath.mp.dps = 35

tol1 = float(sys.argv[1])
tol2 = float(sys.argv[2])
fname1 = sys.argv[3]
fname2 = sys.argv[4]

with open(fname1) as f1:
	txt1 = f1.readlines()

with open(fname2) as f2:
	txt2 = f2.readlines()

assert (len(txt1) == len(txt2))

maxErr = [0, 0]
errCount = 0
line = 0
niterDiffs = [0, 0]
for l in zip(txt1, txt2):
	l0 = l[0].split()  # new, freshly calculated, data
	l1 = l[1].split()  # reference data for comparison
	line += 1
	col = 0
	assert (len(l0) == len(l1))
	for s in zip(l0, l1):
		col += 1
		if (s[0] not in ['f_error', '=', 'texp', 'fU', 'conv', 'mniter', 'total_matvecs', 'matvecs/iter']):
			try:
				f0 = float(s[0])
				f1 = float(s[1])
				err = abs(f0 - f1)
				if (int(err) == err and err > tol1 and line > 387 and line < 488 and col == 4):
					#print("Found niter difference: " + s[0] + " vs. " + s[1])
					niterDiffs[0] += 1
					niterDiffs[1] += int(err)
				else:
					if (line < 387):
						maxErr[0] = max(err, maxErr[0])
					else:
						maxErr[1] = max(err, maxErr[1])
					if ((err > tol1) if (line < 387) else (f0 > (f1 + (2 if line in [489, 490] else 1) * tol2))):
						raise RuntimeError(
						        '\033[91m' + "Error " + str(err) + " larger than " + str(tol1) + " in line: " + str(line) + " col: " +
						        str(col) + '\033[0m'
						)
			except Exception as e:
				print("Exception: " + str(e))
				errCount += 1

# FIXME: totalIters is a fixed value below.
totalIters = 100
print('\033[93m' + "Max error: " + ("%.3g" % maxErr[0]) + (", History: %.3g" % maxErr[1]) + '\033[0m' + ", niter diff count: " + str(niterDiffs[0]) + " avg diff: " + str(niterDiffs[1] / totalIters))
if (errCount != 0):
	print("Failed: errCount=" + str(errCount))
	sys.exit(1)
else:
	print("Test OK.")
