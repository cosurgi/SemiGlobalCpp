/*********************************************************************************
*  2022 Janek Kozicki                                                            *
*  2017 Ido Schaefer - wrote original matlab code, which I (Janek) ported to C++ *
*                                                                                *
*  This program is free software; it is licensed under the terms of the          *
*  GNU General Public License v2 or later. See file LICENSE for details.         *
*********************************************************************************/

// C++ code by Janek Kozicki
// converted from matlab code originally written by Ido Schaefer
// Schaefer2017 "Semi-global approach for propagation of the time-dependent Schrödinger equation for time-dependent and nonlinear problems"
#pragma once
#include "Real.hpp"
#include <boost/optional.hpp>
#include <functional>
//                                                      ↓ ψ from this, yet to be calcualated t1, e.g. Bose-Einstein condensate
//    ↓ -i*H(t)ψ = G(t)ψ                ↓result ψ       ↓                 ↓ t1        ↓ arg ψ
using GOperatorFunction = std::function<VectorXcr(const VectorXcr&, const Real&, const VectorXcr&)>;
// Gdiff_op: A function handle of the form @(u1, t1, u2, t2, more arguments). Returns
// (G(u1, t1) - G(u2, t2))u1. u1 and t1 represent several time-points in
// separated columns, while u2 and t2 represent a single time-point.
// Serves to determine the difference ( H(t1) - H(t2) )ψ1 between reference timestep t2 (the one in the middle of sub-timesteps) and each other timestep t1.
//    ↓ -i*(H(ψ1,t1) - H(ψ2,t2))ψ1    ↓ result ψ      ↓ ψ1              ↓ t1         ↓ arg ψ2          ↓ t2
using GDiffOpFunction = std::function<VectorXcr(const VectorXcr&, const Real&, const VectorXcr&, const Real&)>;
// An arbitrary inhomogeneous function  ↓ result  ↓ time
using InhomogeneousFunc = std::function<VectorXcr(const Real&)>;
using OptionalInhFunc   = boost::optional<InhomogeneousFunc>;
using OptionalGDiffFunc = boost::optional<GDiffOpFunction>;
using OptVecXcr         = boost::optional<VectorXcr>;

Real      largestSingularValue(const MatrixXcr& m); // octave norm
VectorXcr eigenValues(const MatrixXcr& m);          // returns eigenValues of complex matrix

//////////////////////////////////////////////////////////////////////////////////////
// The parameters and helper constants of SemiGlobal algorithm by Schaefer2017      //
//////////////////////////////////////////////////////////////////////////////////////
struct SemiGlobalParams {
	const bool display_mode = true;
	// Setting defaults:
	const int Niter    = 10;            // allowed number of internal iterations in sub-timesteps to achieve convergence
	const int Niter1st = 16;            // as above, but only in the first step - it acn take longer because there was no extrapolation from previous timestep.
	                                    // These two were not used when calculating the reference solution in Schaefer2017 benchmark.
	const int niterWarn = 5;            // after how many sub-timesteps in the self convergence loop it will print warnings
	                                    //
	const int      M_Nt_ts { 0 };       // the number of interior Chebyshev time points in each time-step (M in the article)
	const int      K_Nfm { 0 };         // the number of expansion terms for in computation of function of matrix (K in the article)
	const long int Nu { 0 };            // the size of the grid in spatial representation (number of gridpoints)
	const Real     tol { 0 };           // error tolerance
	const int      Gdiff_matvecs { 0 }; // the number of matrix-vector multiplications used by computation of operation of Gdiff_op
	                                    //
	const bool      Arnoldi { false };  // ORDER DEPENDENCY https://isocpp.org/wiki/faq/ctors#order-dependency-in-members
	const VectorXcr ev_domain {};       // The boundaries of the eigenvalue domain of G, when a Chebyshev algorithm is used
	const Real      dt { 0 };           // timestep
	const int       tmidi { 0 };        // The index of the middle term in the time step // Względem niego s_ext liczy
	const VectorXr  t_ts {};            // sub-timesteps. // The Chebyshev points for expansion in time, in the domain of the time
	const Real      test_tpoint { 0 };  // Test time point for error estimation
	const VectorXr  t_ts_times_4 {};    // used by divdif: For numerical stability, transform time points in timestep to points in interval of length 4
	const VectorXr  t_2ts {};           // all timesteps, from this timestep, from next timestep and with the extra time point used for error estimation
	const MatrixXr  timeMcomp {};       // % Computing the matrix of the time Taylor polynomials.
	const MatrixXr  timeMts {};         // % timeMts contains the points in the current time step
	const MatrixXr  timeMnext {};       // % timeMnext contains the points in the next time step:
	const MatrixXr  Cr2t {};            // coefficients of the transformation from the Newton interpolation polynomial terms, to a Taylor like form
	const VectorXi  s_ext_i {};         // % The indices for the computation of s_ext, bez tmidi, bo względem niego jest liczone. Czyli tam musi być zero

	SemiGlobalParams(
	        const OptVecXcr& ev_dom,     // The boundaries of the eigenvalue domain of G, when a Chebyshev algorithm is used
	        const VectorXcr& ui,         // the initial state vector
	        const Real&      dt_,        // timestep
	        const int&       Nt_ts,      // the number of interior Chebyshev time points in each time-step (M in the article).
	        const int&       Nfm,        // the number of expansion terms for in computation of function of matrix (K in the article)
	        const Real&      tol_,       // desired error tolerance
	        const int&       Gdiff_mvec, // the number of matrix-vector multiplications used by computation of operation of Gdiff_op
	        const int&       niterWarn_  // after how many sub-timesteps in the self convergence loop it will print warnings
	);

	// these are static, because they are called from the class constructor
	static VectorXr getEq117(const int& Nt_ts, const Real& Tts);
	static VectorXi getSExtI(const int& Nt_ts, const int& tmidi);
	static MatrixXr maketM(const VectorXr& t, const int& Nt_ts);
	static MatrixXr r2Taylor4(const VectorXr& x, const Real& Dsize);
};

//////////////////////////////////////////////////////////////////////////////////////
// Two helper classes used for estimation of numerical errors during calculation.   //
//////////////////////////////////////////////////////////////////////////////////////
class SemiGlobalODE;
struct ErrorMessageData {
	const SemiGlobalParams& cpar;
	const Real&             tol;   // tolerance
	const int&              tsi;   // global iterations performed
	const int&              niter; // sub-timesteps iterations over the grid of Chebyshev time-points
	const SemiGlobalODE&    ode;   // used to call getMatvecs() : the global computation effort
};

struct EstimatedErrors {
	// Schaefer2017 alternative names:
	//	texpansion_error	max_texp_error		texp		texp_error	errM_Nt_ts_subTimeSteps
	//	fUerror			max_fUerror		fU				errK_Nfm_funcOfMatrix
	//	Arnoldi_f_error		max_f_error		f_error				errFuncOfMatrix
	//	max_conv_error		reldif			conv_error	conv		errConvergence
	//
	Real errM_Nt_ts_subTimeSteps { 0 }; // The maximal estimated error of U, resulting from sub time-steps         ( parameter M aka Nt_ts              )
	Real errK_Nfm_funcOfMatrix { 0 };   // The maximal estimated error of U, resulting from the function of matrix ( parameter K aka Nfm aka Ncheb      )
	Real errFuncOfMatrix { 0 };         // The maximal estimated error of the function of matrix computation       ( used to calc errK_Nfm_funcOfMatrix )
	Real errConvergence { 0 };          // The maximal estimated convergence error (used as condition to end the sub timesteps loop)

	EstimatedErrors(const Real& arg, Real errConv = 0);
	void updateMaxErrors(const EstimatedErrors& other);
	void print();
	void checkStart(const ErrorMessageData& er);
	void checkInsideSubTimestepLoop(const ErrorMessageData& er);
	void checkAll(const ErrorMessageData& er);
	void checkM_Nt_ts_subTimeSteps(const ErrorMessageData& er);
	void checkK_Nfm_funcOfMatrix(const ErrorMessageData& er);
	void checkFuncOfMatrix(const ErrorMessageData& er);
	void checkConvergence(const ErrorMessageData& er);
	void checkNiterIsEnough(const ErrorMessageData& er);
};

//////////////////////////////////////////////////////////////////////////////////////
// The parameters and helper constants when using the Chebyshev method              //
//////////////////////////////////////////////////////////////////////////////////////
struct CchebData {
	Complex   min_ev, max_ev;
	MatrixXcr Ccheb_f_ts, Ccheb_f_next;

	CchebData(const SemiGlobalParams& cpar, EstimatedErrors& maxErrors);
	// one są static, bo są wołane z konstruktora
	static boost::optional<const CchebData> maybeMakeCchebData(const SemiGlobalParams& cpar, EstimatedErrors& maxErrors);
	static MatrixXcr                        chebcM(const MatrixXcr& fM);
	static MatrixXcr f_chebC(const VectorXr& t, const int& Nfm, const Complex& min_ev, const Complex& max_ev, const Real& tol, const int& Nt_ts);
	static VectorXcr chebc2result(const VectorXcr& Ccheb, const VectorXcr& xdomain, const VectorXcr& xresult);
};


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////                  //////////////////////////////////
//////////////////////////////////    ODE Solver    //////////////////////////////////
//////////////////////////////////                  //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// SemiGlobalODE - and ODE solver class for arbitrary inhomogeneous ODE with source //
// helper classes, used inside:                                                     //
struct EstimatedErrors;  // tracking and estimation of numerical error              //
struct SemiGlobalParams; // helper constants and parameters                         //
struct CchebData;        // helper constants and parameters when using Chebyshev    //
struct DebugResults;     // helper class for debugging                              //
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
struct LocalData;
class SemiGlobalODE {
private:
	friend struct LocalData; // This struct holds the variables which don't need to be remembered between iteration calls. It is defined in SemiGlobal.cpp file.
	// tracking the estimated calculation errors
	EstimatedErrors maxErrors { 0 };     // maximum numerical errors
	EstimatedErrors currentErrors { 0 }; // the errors in last call

	// parameters and helper tables calculated only once. Will need to be recalculated for adaptive schemes which change parameters.
	const SemiGlobalParams                 cpar;
	const boost::optional<const CchebData> cheb;

	// state variables necessary to remember between calls.
	VectorXcr         fiSolution {};  // the wavefunction obtained at the end of current iteration
	MatrixXcr         Uguess {};      // wavefunctions in the sub-timesteps in the future iteration: an extrapolation from current timestep.
	MatrixXcr         Unew {};        // used for recording history.U by calling getDebugUnewInterTimesteps(); NOTE: could be a local variable
	VectorXcr         sNext {};       // inhomogeneous term
	int               allniter { 0 }; // sum of all iterations performed since start
	int               niter0th { 0 }; // niter on 1st call. Used to estimate usual amount of niter. The 1st one is always more costly.
	int               niter { 0 };    // number of iterations spent in the while(…){…} self-consistency loop finding the solution.
	const bool        there_is_ih { false };
	const bool        there_is_Gdiff_op { false };
	GOperatorFunction Gop;
	OptionalGDiffFunc Gdiff_op;
	OptionalInhFunc   ihfun;

public:
	SemiGlobalODE(
	        GOperatorFunction Gop,           // function representing the action of Hamiltonian on psi at given time point: -i*H(t)ψ
	        OptionalGDiffFunc Gdiff_op,      // function representing the difference of action of Hamiltonian in two different time points: -i*(H(t1)ψ1 - H(t2)ψ2)
	        OptionalInhFunc   ihfun,         // function handle which returns the inhomogeneous term vector
	        const OptVecXcr&  ev_domain,     // The boundaries of the eigenvalue domain of G = -i*H, used by Chebyshev method
	        const VectorXcr&  ui,            // the initial state vector
	        const Real&       dt,            // timestep
	        const int&        Nt_ts,         // the number of interior Chebyshev time points in each time-step (M in the article)
	        const int&        Nfm,           // the number of expansion terms for the computation of function of matrix (K in the article)
	        const Real&       tol,           // required error tolerance, determines the number of internal computation self-consistency steps.
	        const int&        Gdiff_matvecs, // The number of matrix-vector multiplications required for computation of operation of Gdiff_op, large scale
	        const int&        niterWarn      // after how many sub-timesteps in the self convergence loop it will print warnings
	);
	void             propagateByDtSemiGlobal(const int& iteration); // takes current iteration number as argument
	const VectorXcr& getFiSolution() { return fiSolution; }

	// debugging
	int                     getMatvecs() const;
	DebugResults            getDebugResults(const int& iteration);
	MatrixXcr               getDebugUnewInterTimesteps();
	const SemiGlobalParams& getSemiGlobalParams();
};

//////////////////////////////////////////////////////////////////////////////////////
// DebugResults                                                                     //
//////////////////////////////////////////////////////////////////////////////////////
struct DebugResults {
	VectorXcr       fiSolution;    // the wavefunction obtained at the end of current iteration
	Real            mniter;        // mniter: the mean number of iteration for a time step, excluding the first step
	int             matvecs;       // matvecs: the number of Hamiltonian operations, large scale computation
	EstimatedErrors maxErrors;     // maximum numerical errors
	int             niter;         // number of iterations spent in the while(…){…} self-consistency loop finding the solution.
	EstimatedErrors currentErrors; // the errors in last call
};

//////////////////////////////////////////////////////////////////////////////////////
// A helper debug structure which contains the history of propagation               //
//////////////////////////////////////////////////////////////////////////////////////
struct History {
	// TODO: maybe use a std::vector<EstimatedErrors>
	VectorXr  t {};          // The time grid of propagation
	MatrixXcr U {};          // The solution at history.t
	VectorXr  texp_error {}; // The estimations for the error of U, resulting from the time-expansion, for all time-steps
	VectorXr  f_error {};    // **Only Arnoldi** The estimations for the error of the computation of the function of matrix for all time-steps
	VectorXr  fUerror {};    // The estimations for the error of U, resulting from the computation of the function of matrix, for all time-steps
	VectorXr  conv_error {}; // The estimations for the convergence errors for all time-steps
	VectorXr  niter {};      // The number of iterations for each time-steps

	// tylko Arnoldi
	bool Arnoldi { false };
	int  saved_Nt_ts { 0 };

	History(const SemiGlobalParams& cpar, const VectorXcr& ui, const int& steps);
	void print();
	void append(const int& tsi, const int& niter_, const EstimatedErrors& currentErrors, const boost::optional<const MatrixXcr> UnewInterTimesteps);
};

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations ← functions which don't have a class yet                    //
//////////////////////////////////////////////////////////////////////////////////////
MatrixXcr f_fun(const VectorXcr& z /* energy domain, spectrum */, const VectorXr& t, const int& Nt_ts, const Real& tol); // CchebData, Ufrom_vArnoldi
MatrixXcr guess0(const VectorXcr& ui, int Np);                                                                           // SemiGlobalODE

template <typename VectZ> MatrixXcr divdif(const VectZ& z, const MatrixXcr& fz); // Ufrom_vArnoldi, SemiGlobalODE

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations ← functions which don't have a class yet                    //
// Chebyshev only                                                                   //
//////////////////////////////////////////////////////////////////////////////////////
template <typename Gop_act_>
MatrixXcr vchebMop(Gop_act_ operatorG, const VectorXcr& u0, const Complex& leftb, const Complex& rightb, const int& Ncheb); // SemiGlobalODE::Chebyshev
MatrixXcr Ufrom_vCheb(
        const MatrixXr&                   timeM,
        const MatrixXcr&                  Ccheb_f,
        const MatrixXcr&                  Vcheb,
        const Real&                       f_error,
        const MatrixXcr&                  v_vecs,
        boost::optional<EstimatedErrors&> err); // SemiGlobalODE::Chebyshev

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations ← functions which don't have a class yet                    //
// Arnoldi only                                                                     //
//////////////////////////////////////////////////////////////////////////////////////
template <typename Gop_act_> /* Hessenberg */ MatrixXcr createKrop(Gop_act_ Op, const VectorXcr& v0, const int& Nv /* Nfm */, MatrixXcr& /* Upsilon */);
Real                                                    get_capacity(const VectorXcr sp, const Complex& testp);
MatrixXcr getRv(const int& Nfm, const MatrixXcr& v_vecs, const int& Nt_ts, const MatrixXcr& Hessenberg, const VectorXcr& samplingp, const Real& capacity);
MatrixXcr Ufrom_vArnoldi(
        const MatrixXr&                   timeM,
        const VectorXcr&                  samplingp,
        const Real&                       capacity,
        const int&                        Nt_ts,
        const Real&                       tol,
        const MatrixXcr&                  RvKr,
        const MatrixXcr&                  Upsilon,
        const int&                        Nfm,
        const MatrixXcr&                  v_vecs,
        boost::optional<EstimatedErrors&> currentErrors); // SemiGlobalODE::Arnoldi
