/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

// This file contains the helper functions to evaluate equations in Tully 1990 and Coker 1993

#include "MultiVector.hpp"
#include "Real.hpp"

std::vector<std::vector<VectorXcr>> getTullyNonAdiabatic(const size_t& points, const Real& length, const Real& startX, int num);

// returns the adiabatic potential and the matrix P from equation 65 in Coker 1993 for each coordinate 'x'
std::pair<std::vector<std::vector<VectorXcr>>, std::vector<MatrixXr>>
getTullyAdiabatic_ObtainedFromEqn66_1993Coker(const size_t& points, const Real& length, const std::vector<std::vector<VectorXcr>>& diabatic);
// I am calculating in the diabatic representation, then to present results I am converting the wavefunction into adiabatic representation.
MultiVectorXcr rotateWaveFunctionEqn69(const MultiVectorXcr wf, const std::vector<MatrixXr>& eqn65matrixP, bool antiRotate = false);
