# SemiGlobalCpp

This repository is a supplementary material to the paper [Very accurate time propagation of coupled Schrödinger equations for femto- and attosecond physics and chemistry, with C++ source code](https://doi.org/10.1016/j.cpc.2023.108839).

SemiGlobalCpp is a C++ implementation of the *semi-global* algorithm, initially published in [[1]](http://dx.doi.org/10.1016/j.jcp.2017.04.017) [[2]](http://dx.doi.org/10.1016/j.jcp.2022.111300).
The original Matlab code is [here](https://github.com/IdoSchaefer/SemiGlobalMATLAB).
The main part is in the files [SemiGlobal.hpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/SemiGlobal.hpp) and [SemiGlobal.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/SemiGlobal.cpp) which contain the actual algorithm.
It is self-contained and can be used independently in other software packages.
This algorithm is supplemented with five sample simulations:

1. Atom in an intense laser field (file [test_atom_laser_ABC.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_atom_laser_ABC.cpp)).
2. Single avoided crossing (file [test_single_dual_crossing.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp)).
3. Dual avoided crossing (file [test_single_dual_crossing.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp)).
4. Gaussian packet in a forced harmonic oscillator (file [test_source_term.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp)).
5. Forced harmonic oscillator with an inhomogeneous source term (file [test_source_term.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp)).

# Description

## Nature of problem

The femto- and attosecond chemistry requires fast and high precision computation tools for quantum dynamics.
Conventional software has problems with providing high precision calculation results (up to 33 significant digits), especially when the computation has to be as fast as possible.

## Solution method

This software fills in the gap by providing the *semi-global* algorithm [[1]](http://dx.doi.org/10.1016/j.jcp.2017.04.017) [[2]](http://dx.doi.org/10.1016/j.jcp.2022.111300) for arbitrary number of coupled electronic states for the time dependent Hamiltonian and nonlinear inhomogeneous source term. It is implemented in a way that allows computation with precision of 15, 18 or 33 significant digits, where the computation speed can be directly controlled by setting the required error tolerance.
The *semi-global* algorithm is implemented in C++ providing a 10 times speed boost compared to original publication of *semi-global* algorithm implemented in Matlab/Octave [[1]](http://dx.doi.org/10.1016/j.jcp.2017.04.017) [[2]](http://dx.doi.org/10.1016/j.jcp.2022.111300).

## Features

* About 10 times faster than the original [Matlab/Octave code](https://github.com/IdoSchaefer/SemiGlobalMATLAB) upon which it was based.
* Supports time dependent Hamiltonian.
* Supports inhomogeneous source term.
* Supports nonlinear problems.
* Supports multiple coupled Schrödinger equations (multiple electronic levels).
* Supports high precision: 18 decimal places (`long double` type) and 33 decimal places ([float128 type](https://www.boost.org/doc/libs/1_81_0/libs/multiprecision/doc/html/boost_multiprecision/tut/floats/float128.html)).
* Supports two time integration methods: Chebyshev and Arnoldi.
* Can be used to produce reference high accuracy solutions of various problems such as:
    * Nonlinear problems.
    * Problems with inhomogeneous source term.
    * Mean field approximation.
    * Gross-Pitaevskii approximation.
    * Scattering problems.

# Requirements

This C++ source code has been run and tested on linux Devuan Chimaera, Debian Bullseye, Debian Bookworm, Ubuntu 20.04 and Ubuntu 22.04.
Older linux distributions couldn't work due to too old version of libeigen library.
See the [Continuous Integration (CI) pipeline](https://gitlab.com/cosurgi/SemiGlobalCpp/-/pipelines) for recent automatic tests.

It should run without significant tweaks on any modern GNU/Linux operating system.

Following packages are required for it to run:

| Package                                                       | Minimal version |
| ------------------------------------------------------------- | --------------- |
| `python`                                                      | 3               |
| `libfftw3-dev libfftw3-double3 libfftw3-long3 libfftw3-quad3` | 3.3.8           |
| `libeigen3-dev`                                               | 3.3.7-2         |
| `libboost-all-dev`                                            | 1.71.0          |

Regarding `libeigen3-dev`: the code may compile with older `libeigen3-dev` version, but calculations are not stable.

Simplest way to install dependencies on Debian or Ubuntu based systems is with command:

```
apt install libfftw3-dev libfftw3-double3 libfftw3-long3 libfftw3-quad3 \
            libeigen3-dev libboost-all-dev
```

# Installation and running

First clone the repository:

```
git clone https://gitlab.com/cosurgi/SemiGlobalCpp.git
```

Next you can run the sample simulations:

```
cd SemiGlobalCpp
make fastSourceTermTest
make fullSourceTermTest
make test_atom_laser_ABC_Double
make test_atom_laser_ABC_Long_double
make test_single_dual_crossing_Double
make test_single_dual_crossing_Long_double
make test_atom_laser_ABC_Float128
make test_single_dual_crossing_Float128
```

> **Warning:**
> the last two simulations running on Float128 precision (the [float128 type](https://www.boost.org/doc/libs/1_81_0/libs/multiprecision/doc/html/boost_multiprecision/tut/floats/float128.html))
> are very slow and can take up to 6 hours. The earlier simulations run reasonably fast in a matter of minutes.

> **Note:**
> The file [makefile](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/makefile) might need some little tweaks to run on other systems.
> For example you might need to install `ccache` or change `CXX=ccache g++` into `CXX=g++` in line 8 of makefile.

The simulations above are discussed in the next section. Each of these `make` commands is running the calculation and compares the results with reference results to confirm that there is no error in the calculation.

Additionally you can plot Figs.6 and 9 from the article which correspond to the reproduced (calculated) Figures 11, 13, 15, 16 from [Coker 1993](http://dx.doi.org/10.1007/978-94-011-1679-4_9) with this command:

```
make plot_Coker
```

and also you can plot the atom in an intense laser field (Fig.3 as colormap from the article) with command:

```
make plot_Atom
```

but please make sure to have `gnuplot` program installed (e.g. with `apt install gnuplot-qt`), since it is used to display the Figures.

![Figures from commands `make plot_Coker` and `make plot_Atom`.](plot-figs.png)


# Sample simulations

There are five sample simulations which use the *semi-global* propagator. They are written in three sample files as follows:

## Forced harmonic oscillator

The [test_source_term.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp) covers four different test cases:

1. Gaussian packet in a forced harmonic oscillator (without source term)  - Chebyshev method.
2. Gaussian packet in a forced harmonic oscillator (without source term)  - Arnoldi method.
3. Forced harmonic oscillator with an arbitrary inhomogeneous source term - Chebyshev method.
4. Forced harmonic oscillator with an arbitrary inhomogeneous source term - Arnoldi method.

This test can be compiled with three different precisions: `double`, `long double` and `float128`, the makefile targets are following:

1. `make test_source_term`
2. `make test_source_term_long_double`
3. `make test_source_term_float128`
4. `make test_source_term_float128_MinTolerance` - an additional target with more strict error tolerance.

The executable, once compiled, takes two arguments:

* First argument (`1` or `2`) selects whether to test with source term (`1`) or without (`2`).
* Second argument (`Czeb` or `Arnoldi`) selects the algorithm variant.

To run the tests run one of these two:

* `make fastSourceTermTest` - fast, about 30 seconds
* `make fullSourceTermTest` - takes about 5 minutes

## Atom in an intense laser field

The [test_atom_laser_ABC.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_atom_laser_ABC.cpp)
is an atom in an intense laser field. Only Arnoldi method can work here. Calculation can be done with various precisions.
The executed test performs a simulation of an atom in an intense laser field and at the end compares the final wavefunction (at time = 1000 a.u.)
with the wavefunction from supplementary materials of paper
["Semi-global approach for propagation of the time-dependent Schrödinger equation for time-dependent and nonlinear problems"](http://dx.doi.org/10.1016/j.jcp.2017.04.017).
The error is in the order of 8e-15 range. To execute the test, use one of these three precisions:

* `make test_atom_laser_ABC_Double`        - takes about 2 minutes
* `make test_atom_laser_ABC_Long_double`   - takes about 7 minutes
* `make test_atom_laser_ABC_Float128`      - takes about 5 hours

## Single and dual avoided crossing

The [test_single_dual_crossing.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp)
test reproduces Figures 11, 13, 15 and 16 from paper ["Computer Simulation Methods for Nonadiabatic Dynamics in Condensed Systems"](http://dx.doi.org/10.1007/978-94-011-1679-4_9)
Again, three numerical precisions are supported. Launch the tests with commands:

* `make test_single_dual_crossing_Double`        - takes about 1 minute
* `make test_single_dual_crossing_Long_double`   - takes about 3 minutes
* `make test_single_dual_crossing_Float128`      - takes about an hour

> **Note:**
> See file [makefile](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/makefile) to examine how the source files are compiled and run by the `make` command.

# Description of files in repository

## The C++ *semi-global* code

* `ArraysTables.cpp`                   - This is the reference data used to reproduce [Schaefer 2017](http://dx.doi.org/10.1016/j.jcp.2017.04.017) laser and atom with Absorbing Boundary Conditions.
* `ArraysTables.hpp`                   - the tables were extracted from files `coulomb_optV240`, `Uex_article` from [Schaefer 2017](http://dx.doi.org/10.1016/j.jcp.2017.04.017).
* `Constants.hpp`                      - helper mathematical or physical constants.
* `FFT.hpp`                            - helper functions to call fast Fourier transform, used by the kinetic energy operator.
* `MultiVector.hpp`, `MultiVector.cpp` - helper mathematical functions to deal with multi-level wavefunction.
* `Real.hpp`                           - declaration of three precision types. Selection of precision type happens via `#define` provided in makefile.
* `SemiGlobal.hpp`, `SemiGlobal.cpp`   - The main SemiGlobal algorithm, it is self-contained and can be used idependently in other software packages.
* `Tully1990_Coker1993_Helpers.hpp`, `Tully1990_Coker1993_Helpers.cpp` - This file contains the helper functions to evaluate equations in [Tully 1990](http://dx.doi.org/10.1063/1.459170) and [Coker 1993](http://dx.doi.org/10.1007/978-94-011-1679-4_9).

## The test framework

* `makefile`                        - the makefile is provided to run all the examples.
* `numDiffSimple.py`, `numDiff.py`  - helper scripts to compare with reference results.

## Example simulations

* `test_atom_laser_ABC.cpp`         - example: the (1) atom in an intense laser field, reproduces results from [Schaefer 2017](http://dx.doi.org/10.1016/j.jcp.2017.04.017).
* `test_single_dual_crossing.cpp`   - example: the (2) single and (3) dual crossing, reproduces results from [Coker 1993](http://dx.doi.org/10.1007/978-94-011-1679-4_9).
* `test_source_term.cpp`            - example: the packet in a (4) harmonic potential (5) or harmonic potential with source term, reproduces results from [Schaefer 2017](http://dx.doi.org/10.1016/j.jcp.2017.04.017).

## Figures

* `plot_Coker_1993.gnuplot`         - the script to show Figs.6 and 9 in article and Figures 11, 13, 15, 16 from [Coker 1993](http://dx.doi.org/10.1007/978-94-011-1679-4_9) once they are calculated with `make plot_Coker`.
* `plot_Atom_Laser.gnuplot`         - the script to show Fig.3 as colormap from the article once it is calculated with `make plot_Atom`.

## License and readme

* `LICENSE`                         - GNU GPL v.2 license.
* `README.md`                       - this file.

## Directories

*  `referenceResults`                - the reference results against which the running tests are compared.
*  `testResults`                     - empty directory, inside it the running tests store their results for comparison.


# Usage

The central part of this repository are the [SemiGlobal.hpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/SemiGlobal.hpp) and [SemiGlobal.cpp](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/SemiGlobal.cpp) files which contain the actual algorithm. Although some examples are provided it is expected that these files will be merged into other software packages to aid in quantum dynamics computations.

To calculate on your own, you have to write your own C++ file similar to one of the accompanying examples to perform a specific computation.
See the FAQ section below for additional details.

<!--- Anonymous reviewer states:
   After reading the paper I could not tell as a
   user, what information I should have about my system to use the code. The Readme
   file only states "To calculate on your own, you have to write your own C++ file
   similar to one of the accompanying examples to perform a specific computation."


   Do I need to provide the Hamiltonian matrix in coordinate representation on a grid?
   Can I use different basis representation?
   How should I define the Hamiltonian and the initial wave function?
   What kind of variables does the user need to prepare and provide for the presented code?

   These should be clarified and clearly communicated both in the paper and in the Readme file.
-->

## Frequently Asked Questions

1. Do I need to provide the Hamiltonian matrix in coordinate representation on a grid?

The representation chosen for a particular problem has to be consistent between the wavefunction, the Hamiltonian acting on it
and the kinetic energy operator.
As long as these three are consistent the coordinate representation chosen can be arbitrary, this is because the *semi-global* algorithm
is agnostic to the implementation details of the Hamiltonian.

2. Can I use different basis representation?

Yes, as mentioned in the previous answer the basis representation used can be chosen arbitrarily as long as both the wavefunction, the Hamiltonian
and the kinetic energy operator work in the same basis. The only restriction being that the wavefunction has to be converted into a single
`VectorXcr` before providing it to the *semi-global* algorithm, like mentioned in subsection 4.1 in the paper.
See for example the usage of function [`flatten`](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp#L171)
in file `test_single_dual_crossing.cpp` which turns several wavefunctions from multiple electronic levels into a single
wavefunction of `VectorXcr` type with which the *semi-global* algorithm can work.
This function is defined in line [144](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/MultiVector.cpp#L144)
of file `MultiVector.cpp`. Similarly a multidimensional (maybe even curvilinear) grid has to be flattened into a single `VectorXcr` type.

3. How should I define the Hamiltonian and the initial wave function?

The Hamiltonian is declared as a function inside the C++ file, see the variable declarations of C++ types `GOperatorFunction` and `GDiffOpFunction`.
The `GOperatorFunction` contains the function handle to the Hamiltonian acting on the wavefunction,
see for example line [113](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L113) and
line [129](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L129) in `test_source_term.cpp`.
The `GDiffOpFunction` contains the function handle to the time dependence of the Hamiltonian,
see for example line [114](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L114) and
line [130](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L130) in `test_source_term.cpp`.
Additionally the inhomogeneous term can be declared using `InhomogeneousFunc`,
see for example line [115](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L115) and
line [131](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L131) in `test_source_term.cpp`.

The wavefunction is declared as a `VectorXcr` type and supplied to the constructor of `SemiGlobalODE`. See lines
[100](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_atom_laser_ABC.cpp#L100) and
[130](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_atom_laser_ABC.cpp#L130) in `test_atom_laser_ABC.cpp`,
lines [295](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp#L295)
and [296](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp#L296)
in `test_single_dual_crossing.cpp` and
lines [104](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L104)
and [133](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L133)
in `test_source_term.cpp`. See also "`const VectorXcr& ui`" in the question below.

4. What kind of variables does the user need to prepare and provide for the presented code?

The required variables are following (listed in order of arguments to the `SemiGlobalODE` constructor, see line
[177](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/SemiGlobal.hpp#L177) in file `SemiGlobal.hpp`):
 
* `GOperatorFunction Gop`

>>    function handle representing the action of Hamiltonian on the wavefunction at given time point: $-i * H(t)\psi$,
>>    see for example line [113](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L113) and
>>    line [129](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L129) in `test_source_term.cpp`.

* `OptionalGDiffFunc Gdiff_op`

>>    function handle representing the difference of action of Hamiltonian in two different time points: $-i * (H(t1)\psi_1 - H(t2)\psi_2)$,
>>    see for example line [114](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L114) and
>>    line [130](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L130) in `test_source_term.cpp`.
>>    This parameter is optional, if there is no time dependence then provide a `OptionalGDiffFunc(boost::none)` argument.

* `OptionalInhFunc   ihfun`

>>    function handle which returns the inhomogeneous term vector,
>>    see for example line [115](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L115) and
>>    line [131](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L131) in `test_source_term.cpp`.
>>    This parameter is optional, if there is no inhomogeneous term then provide a `OptionalInhFunc(boost::none)` argument.

* `const OptVecXcr&  ev_domain`

>>    The boundaries of the eigenvalue domain of $G = -i * H$, used only by the Chebyshev method,
>>    see for example line [123](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L123) and
>>    line [132](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_source_term.cpp#L132) in `test_source_term.cpp`.
>>    This parameter is optional, <!--- if Arnoldi method is used then provide a `OptVecXcr(boost::none)` argument. -->
>>    providing `OptVecXcr(boost::none)` means that Arnoldi method is used by the *semi-global* algorithm.

* `const VectorXcr&  ui`

>>    the initial state vector (initial wavefunction). If multidimensional or multi-level or any other coordinate
>>    representation is used for calculations then here the "flattened" version of the wavefunction has to be provided.
>>    See for example lines [295](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp#L295)
>>    and [296](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/SemiGlobalCpp/test_single_dual_crossing.cpp#L296)
>>    in `test_single_dual_crossing.cpp`. See also earlier question number 2 in this FAQ.

* `const Real&       dt`

>>    the timestep used in the calculations,

* `const int&        Nt_ts`

>>    the number of interior Chebyshev time points in each time-step ($M$ in the article),

* `const int&        Nfm`

>>    the number of expansion terms used for the computation of the function of the matrix ($K$ in the article),

* `const Real&       tol`

>>    required error tolerance, determines the number of internal computation self-consistency steps,

* `const int&        Gdiff_matvecs`

>>    The number of matrix-vector multiplications required for computation of operation of `Gdiff_op`,
>>    This parameter is used only for estimating the computation cost. It does not affect the simulation.

* `const int&        niterWarn`

>>    after how many internal iterations in the self convergence loop the calculation loop will start to print warnings
>>    that there are too many internal iterations in a single timestep.
>>    This argument is intended to be used for fine-tuning the simulation parameters $M$, $K$ and tolerance `tol`.

# Support

Please open an [issue](https://gitlab.com/cosurgi/SemiGlobalCpp/-/issues) or [contact me](https://pg.edu.pl/p/jan-kozicki-19725) directly if you wish to communicate.

# Contributing

Please open a [merge request](https://gitlab.com/cosurgi/SemiGlobalCpp/-/merge_requests) or open an [issue](https://gitlab.com/cosurgi/SemiGlobalCpp/-/issues) if you wish to contribute code.
The current [.gitlab-ci.yml](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/.gitlab-ci.yml) file is running tests in the [Continuous Integration (CI) pipeline](https://gitlab.com/cosurgi/SemiGlobalCpp/-/pipelines), they have to pass in a merge request. Also a new code should have corresponding tests.

# Authors and acknowledgment

The C++ code is written by me, [Janek Kozicki](https://pg.edu.pl/p/jan-kozicki-19725),
the original Matlab code was written by [Ido Schaefer](https://github.com/IdoSchaefer) and I wish to extend my thanks for this Matlab implementation.

# License

The license is [GNU GPLv2](https://gitlab.com/cosurgi/SemiGlobalCpp/-/blob/main/LICENSE).

# Project status

This project is under development as of year 2023.



<!-- ## Example README below, quite useful.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

Soon a three electronic levels simulations of NaRb excited by a laser impulse will be added.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

-->
