/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

// This file contains the helper functions to evaluate equations in Tully 1990 and Coker 1993

#include "Tully1990_Coker1993_Helpers.hpp"

// parameters used by Tully 1990 and Coker 1993
constexpr int levels = 2; // we have two coupled potentials

// single crossing
constexpr Real A = 0.01;
constexpr Real B = 1.6;
constexpr Real C = 0.005;
constexpr Real D = 1.0;

Real potTully1_00(Real x)
{
	using std::exp;
	return (x > 0) ? (A * (1 - exp(-B * x))) : (-A * (1 - exp(B * x)));
}

Real potTully1_01(Real x)
{
	using std::exp;
	return C * exp(-D * x * x);
}

// double crossing
constexpr Real A_2 = 0.10;
constexpr Real B_2 = 0.28;
constexpr Real C_2 = 0.015;
constexpr Real D_2 = 0.06;
constexpr Real E_2 = 0.05;

Real potTully2_11(Real x)
{
	using std::exp;
	return -A_2 * exp(-B_2 * x * x) + E_2;
}

Real potTully2_01(Real x)
{
	using std::exp;
	return C_2 * exp(-D_2 * x * x);
}

std::vector<std::vector<VectorXcr>> getTullyNonAdiabatic(const size_t& points, const Real& length, const Real& startX, int num)
{
	Real dx = length / points;

	assert(num == 1 or num == 2); // Tully potential single crossing == 1, double crossing == 2.

	std::vector<std::vector<VectorXcr>> potentialMatrix(levels, std::vector<VectorXcr>(levels, VectorXr::Zero(points)));
	for (size_t i = 0; i < points; i++) {
		Real x                   = startX + i * dx;
		potentialMatrix[0][0](i) = num == 1 ? potTully1_00(x) : 0;
		potentialMatrix[1][1](i) = num == 1 ? -potentialMatrix[0][0](i) : potTully2_11(x);

		potentialMatrix[0][1](i) = num == 1 ? potTully1_01(x) : potTully2_01(x);
		potentialMatrix[1][0](i) = potentialMatrix[0][1](i);
	}
	return potentialMatrix;
}

// returns the adiabatic potential and the matrix P from equation 65 in Coker 1993 for each coordinate 'x'
std::pair<std::vector<std::vector<VectorXcr>>, std::vector<MatrixXr>>
getTullyAdiabatic_ObtainedFromEqn66_1993Coker(const size_t& points, const Real& length, const std::vector<std::vector<VectorXcr>>& diabatic)
{
	Real dx = length / points;

	std::vector<std::vector<VectorXcr>> potentialAdiabaticMatrix(levels, std::vector<VectorXcr>(levels, VectorXr::Zero(points)));
	std::vector<MatrixXr>               eqn65matrixP(points, MatrixXr::Zero(2, 2));
	VectorXcr                           prevPsi2(2);
	bool                                prevSwap = false;
	for (size_t i = 0; i < points; i++) {
		MatrixXr pot(2, 2);
		pot(0, 0) = diabatic[0][0](i).real();
		pot(0, 1) = diabatic[0][1](i).real();
		pot(1, 0) = diabatic[1][0](i).real();
		pot(1, 1) = diabatic[1][1](i).real();
		Eigen::EigenSolver<MatrixXr> es(pot);
		Real                         e1  = es.eigenvalues()(0).real();
		Real                         e2  = es.eigenvalues()(1).real();
		bool                         swp = (e1 > e2);
		if (swp) std::swap(e1, e2);
		potentialAdiabaticMatrix[0][0](i) = e1;
		Complex D12                       = 0;
		if (i != 0) {
			VectorXcr psi1 = es.eigenvectors().col(0);
			VectorXcr psi2 = es.eigenvectors().col(1);

			if (not swp) {
				eqn65matrixP[i](0, 0) = psi1(0).real();
				eqn65matrixP[i](1, 0) = psi1(1).real();
				eqn65matrixP[i](0, 1) = psi2(0).real();
				eqn65matrixP[i](1, 1) = psi2(1).real();
			} else {
				// swap rows because I swapped the eigenvalues
				eqn65matrixP[i](1, 0) = psi1(0).real();
				eqn65matrixP[i](0, 0) = psi1(1).real();
				eqn65matrixP[i](1, 1) = psi2(0).real();
				eqn65matrixP[i](0, 1) = psi2(1).real();
			}
			{
				if (eqn65matrixP[i](0, 0) < 0) eqn65matrixP[i](0, 0) *= -1;
				if (eqn65matrixP[i](1, 0) > 0) eqn65matrixP[i](1, 0) *= -1;
				if (eqn65matrixP[i](0, 1) < 0) eqn65matrixP[i](0, 1) *= -1;
				if (eqn65matrixP[i](1, 1) < 0) eqn65matrixP[i](1, 1) *= -1;
			}
			assert(psi1(0).imag() == 0);
			assert(psi1(1).imag() == 0);
			assert(psi2(0).imag() == 0);
			assert(psi2(1).imag() == 0);

			// calculate D12
			if (swp) std::swap(psi1, psi2);
			D12      = Complex((swp == prevSwap ? 1. : -1.) * (swp ? -1. : 1.)) * ((psi1.dot((psi2 - prevPsi2)) / dx));
			prevPsi2 = psi2;
		} else {
			prevPsi2              = es.eigenvectors().col(1);
			eqn65matrixP[i](0, 0) = 1;
			eqn65matrixP[i](1, 0) = 0;
			eqn65matrixP[i](0, 1) = 0;
			eqn65matrixP[i](1, 1) = 1;
		}
		potentialAdiabaticMatrix[0][1](i) = D12.real();
		potentialAdiabaticMatrix[1][0](i) = D12.real();
		potentialAdiabaticMatrix[1][1](i) = e2;

		assert(D12.imag() == 0);
		assert(es.eigenvalues()(0).imag() == 0);
		assert(es.eigenvalues()(1).imag() == 0);

		prevSwap = swp;
	}
	return { potentialAdiabaticMatrix, eqn65matrixP };
}

// I am calculating in the diabatic representation, then to present results I am converting the wavefunction into adiabatic representation.
MultiVectorXcr rotateWaveFunctionEqn69(const MultiVectorXcr wf, const std::vector<MatrixXr>& eqn65matrixP, bool antiRotate)
{
	MultiVectorXcr ret = getZeroMultiVectorXcr(wf);
	for (int i = 0; i < wf[0].size(); i++) {
		MatrixXcr rot = eqn65matrixP[i];
		VectorXcr wff(2);
		wff(0) = wf[0](i);
		wff(1) = wf[1](i);
		VectorXcr rotated(2);
		if (antiRotate) {
			// acting with operator P⁺ψ
			rotated = rot.transpose() * wff;
		} else {
			// acting with operator P ψ
			rotated = rot * wff;
		}
		ret[0](i) = rotated(0);
		ret[1](i) = rotated(1);
	}
	return ret;
}
