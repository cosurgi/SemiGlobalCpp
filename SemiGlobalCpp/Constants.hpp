/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

// see also https://gitlab.com/yade-dev/trunk/-/blob/master/lib/high-precision/Constants.hpp
#pragma once
#include "Real.hpp"
#include <boost/math/constants/constants.hpp>

namespace math {
template <typename Rr> const constexpr bool useConstexprConstants = false;

// const everywhere
template <int N> struct ConstConstantsHP {
	static const Real    HALF_PI;
	static const Real    PI;
	static const Real    TWO_PI;
	static const Real    PI_SQUARED; // pi^2
	static const Real    E;
	static const Complex I;
};
template <int N> const Real    ConstConstantsHP<N>::HALF_PI    = boost::math::constants::half_pi<Real>();
template <int N> const Real    ConstConstantsHP<N>::PI         = boost::math::constants::pi<Real>();
template <int N> const Real    ConstConstantsHP<N>::TWO_PI     = boost::math::constants::two_pi<Real>();
template <int N> const Real    ConstConstantsHP<N>::PI_SQUARED = boost::math::constants::pi_sqr<Real>();
template <int N> const Real    ConstConstantsHP<N>::E          = boost::math::constants::e<Real>();
template <int N> const Complex ConstConstantsHP<N>::I          = Complex(0, 1);

template <int N> using ConstantsHP = ConstConstantsHP<N>;

// constexpr
template <int N> struct ConstexprConstantsHP {
	static constexpr Real hbar    = 1;
	static constexpr Real hbarSqr = 1; // ℏ²
};
template <int N> constexpr Real ConstexprConstantsHP<N>::hbar;
template <int N> constexpr Real ConstexprConstantsHP<N>::hbarSqr;
//template <int N> constexpr Real ConstexprConstantsHP<N>::m;

template <int N> using PhysConstantsHP = ConstexprConstantsHP<N>;

}

using Mathr     = math::ConstantsHP<1>;
using PhysConst = math::PhysConstantsHP<1>;
