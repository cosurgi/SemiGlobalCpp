# gnuplot

set title 'Model atom in an intense laser field, Fig.3 in the article (plotted as colormap)'
set lmargin at screen 0.12
set rmargin at screen 0.81

set pm3d map
set log cb
set xr [-240:240]

set xlabel "R [a_0]"
set ylabel "Time [a.u.]"
set cblabel "|{/Symbol y}(R)|^2"

splot './testResults/atom_laser.txt' u ($1*480/768.0-240):($2*5):3 matrix with image notitle

pause -1

