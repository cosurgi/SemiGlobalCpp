/*************************************************************************
*  2022 Janek Kozicki                                                    *
*  2017 Ido Schaefer - original matlab code, which I ported to C++       *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#include "Constants.hpp"
#include "FFT.hpp"
#include "SemiGlobal.hpp"
#include <boost/core/demangle.hpp>
#include <iostream>

VectorXcr Hpsi(VectorXr K, VectorXr V, VectorXcr psi)
{
	VectorXcr result(psi.size());
	doFFT(psi, result);
	result = result.array() * K.array();
	doIFFT(result, result);
	result = result.array() + V.array() * psi.array();
	return result;
}

VectorXr someIhfun(Real t, const VectorXr& x)
{ // An arbitrary inhomogeneous function
	return (-x.array().pow(2.) / 2.).exp() * cos(t);
};

VectorXcr minus_i_Hpsi(
        const VectorXcr& /*u*/, // current (or future, depending how you look at it) wavefunction (read-only) used in eg. Bose-Einstein Condensate trap
        const Real&      tt,    // time
        const VectorXcr& v,     // wavefunction from previous timestep. This is the input for the Hamiltonian
        const VectorXr&  K,     // kinetic energy matrix diagonal in the p domain
        const VectorXr&  V,     // potential energy matrix diagonal in the x domain
        const VectorXr&  x      // positional representation
)
{
	return -Mathr::I * Hpsi(K, V.array() + x.array() * cos(tt), v);
};

// Gdiff_op=@(u1, t1, u2, t2) -1i*x*(cos(t1) - ones(1, Nt_ts)*cos(t2)).*u1
//   u1 and t1 can represent the time-points from the sub-timesteps
//   while u2 and t2 represent a single time-point in the middle.
// returns the time dependent part of the Hamiltonian, meaning:
// -i*(H(u1,t1) - H(u2,t2))u1 = -i*(H(t1)-H(t2))ψ1 // so this is the difference between two Hamiltonians acting on ψ
VectorXcr GDiffOp(VectorXcr u1, Real t1, VectorXcr /* u2 */, Real t2, const VectorXr& x)
{
	using std::cos;
	return -Mathr::I * (x * (cos(t1) - cos(t2))).array() * u1.array();
};

int main(int argc, char* argv[])
{
	std::cerr << "Using precision " << boost::core::demangle(typeid(Real).name()) << "\n";
	int  doTest  = 0;
	bool Arnoldi = false;
	if (argc == 3) {
		// doTest == 1 - will run matlab example test_source_term.m from Schaefer2017 supplementary materials
		// doTest == 2 - will run matlab example test_harmonic.m    from Schaefer2017 supplementary materials
		doTest  = boost::lexical_cast<int>(argv[1]);
		Arnoldi = (argv[2] == std::string("Arnoldi"));
		std::cerr << "will test " << doTest << "  " << (doTest == 2 ? std::string("HARMONIC") : std::string("SourceTerm ihfun")) << "\n";
		std::cerr << "Arnoldi = " << std::boolalpha << Arnoldi << "\n";
		if (doTest < 1 or doTest > 2) exit(1);
	}

	Real T = 10.0;      // end_time
#ifdef SELECT_MIN_TOLERANCE // extra compilation option to run tests with smaller error tolerance
	static_assert(std::is_same<Real, boost::float128_t>::value);
	int  Nt_ts = 21;    // interior_time_subpoints (M in the article)
	int  Ncheb = 37;    // the number of expansion terms for the computation of the function of matrix (K in the article)
	Real tol   = 1e-25; // target error tolerance
	std::cerr << "LOW     error tolerance tol=" << tol << " Ncheb=" << Ncheb << " Nt_ts=" << Nt_ts << " and huge Niter, Niter1st\n";
#else
	int  Nt_ts = 9;    // interior time subpoints
	int  Ncheb = 15;   // the number of expansion terms for the function of matrix (K in the article)
	Real tol   = 1e-5; // target error tolerance
	std::cerr << "Regular error tolerance tol=" << tol << " Ncheb=" << Ncheb << " Nt_ts=" << Nt_ts << "\n";
#endif

	// Constructing the grid:

	Real     L  = 16 * sqrt(Mathr::PI);
	int      Nx = 128;
	Real     dx = L / Nx;
	VectorXr x(Nx); // x = (-L/2:dx:(L/2 - dx)).';   // this is the positional gird
	for (int i = 0; i < x.size(); i++) {
		x(i) = -L / 2 + dx * i;
	}
	VectorXr p(Nx); // (0:(2*pi/L):(2*pi*(1/dx - 1/L))).';  // Constructing the kinetic energy matrix diagonal in the p domain
	for (int i = 0; i < p.size(); i++) {
		p(i) = i * 2 * Mathr::PI / L;
		if (i >= Nx / 2) p(i) -= 2 * Mathr::PI / dx;
	}
	VectorXr K(Nx); // Constructing the kinetic energy matrix diagonal in the p domain
	for (int i = 0; i < K.size(); i++) {
		K(i) = p(i) * p(i) / 2;
	}
	VectorXr V(Nx); // The potential energy matrix diagonal in the x domain
	for (int i = 0; i < V.size(); i++) {
		V(i) = x(i) * x(i) / 2;
	}
	VectorXcr fi0(Nx); // The harmonic oscillator ground state
	for (int i = 0; i < fi0.size(); i++) {
		using std::exp;
		using std::pow;
		using std::sqrt;
		fi0(i) = pow(Mathr::PI, Real(-0.25)) * exp(-x(i) * x(i) / 2) * sqrt(dx);
	}
	using namespace std::placeholders;
	// The function handles
	GOperatorFunction Gop      = std::bind(minus_i_Hpsi, _1, _2, _3, K, V, x); // -i* H(t)ψ
	GDiffOpFunction   Gdiff_op = std::bind(GDiffOp, _1, _2, _3, _4, x);        // -i*(H(ψ1,t1) - H(ψ2,t2))ψ1
	InhomogeneousFunc ihfun    = std::bind(someIhfun, _1, x);                  // An arbitrary inhomogeneous function

	// The output time-grid
	Real dt    = Real(1) / Real(10); // careful, writing just double(0.1) would produce 0.100000000000000005551115123125783 for float128 !
	int  steps = int(std::round(T / dt));

	// The number of matrix-vector multiplications required for the computation of the operation of Gdiff_op for each time-point (usually less than 2).
	int       Gdiff_matvecs = 0;
	VectorXcr ev_domain(2); // energy eigenvalues domain rainge, for Chebyshev method.
	ev_domain(0) = Real(-188) * Mathr::I;
	ev_domain(1) = Mathr::I;

	// propagateByDtSemiGlobal(@(u, tgrid, v) -1i*Hpsi(K, V + x*cos(tgrid), v), @(u1, t1, u2, t2) -1i*x*(cos(t1) - ones(1, Nt_ts)*cos(t2)).*u1, 0, ihfun, [-188*1i, 1i], fi0, tgrid, Nts, Nt_ts, Ncheb, tol);
	SemiGlobalODE ode(
	        Gop,
	        Gdiff_op,
	        doTest == 2 ? OptionalInhFunc(boost::none) : ihfun,
	        Arnoldi ? OptVecXcr(boost::none) : ev_domain,
	        fi0,
	        dt,
	        Nt_ts,
	        Ncheb,
	        tol,
	        Gdiff_matvecs,
	        5);
	int                     iteration = 0;
	const SemiGlobalParams& cpar      = ode.getSemiGlobalParams();
	History                 history(cpar, fi0, steps);
	MatrixXcr               U { MatrixXcr::Zero(cpar.Nu, steps + 1) };
	U.col(0) = fi0;
	for (iteration = 0; iteration < steps; iteration++) {
		ode.propagateByDtSemiGlobal(iteration);
		auto dbg             = ode.getDebugResults(iteration); // extracting all debug data.
		U.col(iteration + 1) = dbg.fiSolution;
		history.append(iteration, dbg.niter, dbg.currentErrors, /* ode.getDebugUnewInterTimesteps() */ boost::none);
	}
	auto dbg = ode.getDebugResults(iteration);
	// % The mean number of iterations, for a time step (should be close to 1, for ideal efficiency):
	std::cerr << "mniter        = " << dbg.mniter << "\n"; //  (average number of sub-iterations per dt)
	// % matvecs: The number of matrix-vector multiplications, which constitutes the most of the numerical effort (The number of matrix-vector multiplications is counted as the number of
	// % large scale operations with the highest scaling with the dimension of the problem.)
	std::cerr << "total matvecs = " << dbg.matvecs << "\n";
	std::cerr << "matvecs/iter  = " << Real(dbg.matvecs) / (steps + /* see note below */ 1) << "\n\n";

	if (doTest != 0) {
#ifdef SELECT_MIN_TOLERANCE
		std::cout << std::setprecision(25) << std::scientific << U.cwiseAbs() << "\n\n";
		std::cout << std::setprecision(25) << std::scientific << U.real() << "\n\n";
		std::cout << std::setprecision(25) << std::scientific << U.imag() << "\n\n";
#else
		std::cout << std::setprecision(15) << std::scientific << U.cwiseAbs() << "\n\n";
		std::cout << std::setprecision(15) << std::scientific << U.real() << "\n\n";
		std::cout << std::setprecision(15) << std::scientific << U.imag() << "\n\n";
#endif
		history.print();
		dbg.maxErrors.print();
		std::cout << "mniter        = " << dbg.mniter << "\n"; //  (average number of sub-iterations per dt)
		std::cout << "total_matvecs = " << dbg.matvecs << "\n";
		std::cout << "matvecs/iter  = " << Real(dbg.matvecs) / (steps /* this +1 is to reproduce old tests, but it was a small mistake */ + 1)
		          << "\n\n";
		//std::cout << history.U << "\n\n";
	}
}
