/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#pragma once
#include "Real.hpp"
#include <vector>

struct ArraysTables {
	// data used to reproduce Schaefer2017 laser and atom with Absorbing Boundary Conditions ABC
	// the tables were extracted from files coulomb_optV240, Uex_article from http://dx.doi.org/10.1016/j.jcp.2017.04.017
	static std::vector<Real>    x240;               // ✓ R coordinate in positional representation. The coordinate of each node in the tables
	static std::vector<Real>    K240;               // ✓ the kinetic energy matrix diagonal in the p domain
	static std::vector<Complex> Vabs240;            // ✓ The potential energy matrix diagonal in the x domain
	static std::vector<Real>    xabs240;            // ✓ Laser potential, capped at the ends
	static std::vector<Complex> fi0240;             // ✓ The atom ground state
	static std::vector<Complex> solution_at_T_1000; // ✓ The reference solution at t=1000 a.u.
};
