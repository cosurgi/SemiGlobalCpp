/*************************************************************************
*  2022 Janek Kozicki                                                    *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#pragma once
#define BOOST_CSTDFLOAT_NO_LIBQUADMATH_COMPLEX
#include <boost/cstdfloat.hpp> // must be the first include
//
#include <Eigen/Dense>
#include <complex>

#ifdef SELECT_FLOAT128
using Real = boost::float128_t;
#elif SELECT_LONG_DOUBLE
using Real = long double;
#elif SELECT_DOUBLE
using Real = double;
#else
#error "Select precision"
#endif

using Complex = std::complex<Real>;
using VectorXcr = Eigen::Matrix<Complex, Eigen::Dynamic, 1>;
using VectorXr  = Eigen::Matrix<Real, Eigen::Dynamic, 1>;
using VectorXi  = Eigen::Matrix<int, Eigen::Dynamic, 1>;
using MatrixXcr = Eigen::Matrix<Complex, Eigen::Dynamic, Eigen::Dynamic>;
using MatrixXr  = Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic>;

#ifndef BOOST_MATH_USE_FLOAT128 // defined in cstdfloat.hpp
#error "Use -DBOOST_MATH_USE_FLOAT128"
#endif
