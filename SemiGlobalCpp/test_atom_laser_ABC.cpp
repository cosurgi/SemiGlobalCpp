/*************************************************************************
*  2022 Janek Kozicki                                                    *
*  2017 Ido Schaefer - original matlab code, which I ported to C++       *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#include "ArraysTables.hpp"
#include "Constants.hpp"
#include "FFT.hpp"
#include "SemiGlobal.hpp"
#include <boost/core/demangle.hpp>
#include <iostream>

VectorXcr Hpsi(VectorXr K, VectorXcr V, VectorXcr psi)
{
	VectorXcr result(psi.size());
	doFFT(psi, result);
	result = result.array() * K.array();
	doIFFT(result, result);
	result = result.array() + V.array() * psi.array();
	return result;
}

Real funcTime(const Real& t)
{
	using std::cos;
	using std::cosh;
	using std::pow;
	static Real o1 = Real(1) / Real(10); // because double(0.1) converted to float128 produces 0.100000000000000005551115123125783 in 33 decimal places
	static Real o6 = Real(6) / Real(100);
	return -o1 * pow((Real(1) / cosh((t - 500) / (170))), 2) * cos(o6 * (t - 500));
}

VectorXcr minus_i_Hpsi(
        const VectorXcr& /*u*/,  // wavefunction from this timestep (read-only) used in e.g. Bose-Einstein Condensate trap
        const Real&      t,      // time
        const VectorXcr& v,      // wavefunction from previous timestep. This is on what the Hamiltonian acts
        const VectorXr&  K,      // kinetic energy matrix diagonal in the p domain
        const VectorXcr& V,      // potential energy matrix diagonal in the x domain
        const VectorXr&  xabs240 // the laser electric field, capped at the ends
)
{
	return -Mathr::I * Hpsi(K, V.array() + xabs240.array() * funcTime(t), v);
};

// Gdiff_op=@(u1, t1, u2, t2) -1i*x*(cos(t1) - ones(1, Nt_ts)*cos(t2)).*u1
//   u1 and t1 can represent the time-points from the sub-timesteps
//   while u2 and t2 represent a single time-point in the middle.
// returns the time dependent part of the Hamiltonian
// -i*(H(u1,t1) - H(u2,t2))u1 = -i*(H(t1)-H(t2))ψ1 // so that is the difference between the Hamiltonians at different timesteps acting on ψ
VectorXcr GDiffOp(VectorXcr u1, Real t1, VectorXcr /* u2 */, Real t2, const VectorXr& xabs240)
{
	return -Mathr::I * (xabs240 * (funcTime(t1) - funcTime(t2))).array() * u1.array();
};

int main(int argc, char* argv[])
{
	bool printWavefunction = false; // optionally for plotting (as colormap) the Fig.3 in the paper
	if ((argc == 2) and (argv[1] == std::string("printWavefunction"))) {
		printWavefunction = true;
	}

	std::cerr << "Using precision " << boost::core::demangle(typeid(Real).name()) << "\n";
	Real T     = 1000.0;                               // end_time
	int  Nt_ts = 9;                                    // interior_time_subpoints
	int  Nkr   = 9;                                    // the number of expansion terms for the function of matrix
	Real eps   = std::numeric_limits<Real>::epsilon(); //
	Real tol   = eps;                                  // target error tolerance
	std::cerr << "\n T=" << T << " Nt_ts=" << Nt_ts << " Nkr=" << Nkr << " tol=" << tol << "\n";

	// Constructing the grid:
	int Nx = 768; // commonConfig.points

	// Alternative generating loops to initialise the data, by default I am using the exact values (below) as in Schaefer2017 paper, see also '(*)' below.
//#define NO_EXACT_COPY_FROM_Schaefer2017
#ifdef NO_EXACT_COPY_FROM_Schaefer2017
	Real     L  = 480;
	Real     dx = L / Nx; // deltaX
	VectorXr x240(Nx);
	for (int i = 0; i < x240.size(); i++) {
		x240(i) = -L / 2 + dx * i;
		assert(abs(ArraysTables::x240[i] - x240(i)) < eps);
	}
	VectorXr p(Nx);
	for (int i = 0; i < p.size(); i++) {
		p(i) = i * 2 * Mathr::PI / L;
		if (i >= Nx / 2) p(i) -= 2 * Mathr::PI / dx;
	}
	VectorXr K(Nx); // Constructing the kinetic energy matrix diagonal in the p domain
	for (int i = 0; i < K.size(); i++) {
		K(i) = p(i) * p(i) / 2;
		assert(abs(ArraysTables::K240[i] - K(i)) < 25 * eps);
	}
	VectorXcr Vabs240(Nx); // The potential energy matrix diagonal in the x domain
	for (int i = 0; i < Vabs240.size(); i++) {
		Vabs240(i) = ArraysTables::Vabs240[i];
	}
	VectorXcr fi0240(Nx); // The atom ground state
	for (int i = 0; i < fi0240.size(); i++) {
		fi0240(i) = ArraysTables::fi0240[i];
	}
	VectorXr xabs240(Nx); // Laser potential, capped at the ends.
	for (int i = 0; i < xabs240.size(); i++) {
		xabs240(i) = ArraysTables::xabs240[i];
	}
#else
	VectorXr  x240    = Eigen::Map<VectorXr>(ArraysTables::x240.data(), Nx);
	VectorXr  K       = Eigen::Map<VectorXr>(ArraysTables::K240.data(), Nx);     // Constructing the kinetic energy matrix diagonal in the p domain
	VectorXcr Vabs240 = Eigen::Map<VectorXcr>(ArraysTables::Vabs240.data(), Nx); // The potential energy matrix diagonal in the x domain
	VectorXcr fi0240  = Eigen::Map<VectorXcr>(ArraysTables::fi0240.data(), Nx);  // The atom ground state
	VectorXr  xabs240 = Eigen::Map<VectorXr>(ArraysTables::xabs240.data(), Nx);  // Laser potential, capped at the ends.
#endif
	using namespace std::placeholders;
	// The function handles
	GOperatorFunction Gop      = std::bind(minus_i_Hpsi, _1, _2, _3, K, Vabs240, xabs240); // -i* H(t)ψ
	GDiffOpFunction   Gdiff_op = std::bind(GDiffOp, _1, _2, _3, _4, xabs240);              // -i*(H(ψ1,t1) - H(ψ2,t2))ψ1

	// The timestep and number of iteration steps
	// PhD preliminary benchmark: dt = Real(25) / Real(6000);
	Real dt    = Real(25) / Real(1000);
	int  steps = int(std::round(T / dt));

	// The number of matrix-vector multiplications required for the computation of the operation of Gdiff_op for each time-point (usually less than 2).
	int Gdiff_matvecs = 0;

	// propagateByDtSemiGlobal(@(u, tgrid, v) -1i*Hpsi(K, V + x*cos(tgrid), v), @(u1, t1, u2, t2) -1i*x*(cos(t1) - ones(1, Nt_ts)*cos(t2)).*u1, 0, ihfun, [-188*1i, 1i], fi0, tgrid, Nts, Nt_ts, Nkr, tol);
	// [u, ~, matvecs, all_est_er] = SemiGlobalArnoldi_xp(K240, Vabs240, @(u,x,t) -xabs240*0.1*sech((t-500)/(170)).^2.*cos(0.06*(t-500)), [], fi0240, x240, [0 T], Nt, Nt_ts, Nkr, eps, Niter, 20, false);
	SemiGlobalODE ode(Gop, Gdiff_op, OptionalInhFunc(boost::none), OptVecXcr(boost::none), fi0240, dt, Nt_ts, Nkr, tol, Gdiff_matvecs, 7);
	int           iteration = 0;
	for (iteration = 0; iteration < steps; iteration++) {
		ode.propagateByDtSemiGlobal(iteration); // propagate by single timestep dt
		if (iteration % 1000 == 0) std::cerr << iteration << " / " << steps << "\n";
		// given argument "printWavefunction" print the |ψ|² every 200 iterations
		if (printWavefunction and (iteration % 200 == 0)) {
			std::cout << ode.getFiSolution().transpose().array().abs().real().pow(2) << "\n";
		}
	}

	VectorXcr solution = ode.getFiSolution();
	//std::cerr << std::setprecision(19) << std::scientific << solution << "\n\n";

	// now compare with the reference result
	Real maxErrInEps  = 0;
	Real maxErrInEpsD = 0; // the error calculated using the double ULP error
	for (int i = 0; i < solution.size(); i++) {
		if (std::is_same<Real, double>::value) {
#ifdef NO_EXACT_COPY_FROM_Schaefer2017
			assert(abs(ArraysTables::solution_at_T_1000[i] - solution(i)) < 156 * eps); // Note: use "156 * " if using the generating loops above (*)
#else
			assert(abs(ArraysTables::solution_at_T_1000[i] - solution(i)) < 39 * eps); // Note: use "156 * " if using the generating loops above (*)
#endif
		}
		maxErrInEps  = std::max((abs(ArraysTables::solution_at_T_1000[i] - solution(i)) / eps), maxErrInEps);
		maxErrInEpsD = std::max((abs(ArraysTables::solution_at_T_1000[i] - solution(i)) / 2.22045e-16), maxErrInEpsD);
	}
	std::cerr << "max error in Eps units: " << maxErrInEps << "\n\n";
	std::cerr << "max error in Eps (double) units: " << maxErrInEpsD << "\n\n";
	std::cerr << "Test OK\n";
}
