/*********************************************************************************
*  2022 Janek Kozicki                                                            *
*  2017 Ido Schaefer - wrote original matlab code, which I (Janek) ported to C++ *
*                                                                                *
*  This program is free software; it is licensed under the terms of the          *
*  GNU General Public License v2 or later. See file LICENSE for details.         *
*********************************************************************************/

#include "SemiGlobal.hpp"
#include "Constants.hpp"
#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/math/special_functions/factorials.hpp>
#include <iostream>

// implementing matlab norm
#include <Eigen/SVD>
// Use https://eigen.tuxfamily.org/dox/classEigen_1_1JacobiSVD.html because octave manual says that norm(matrix) returns "Largest singular value of A."
Real largestSingularValue(const MatrixXcr& m) { return Eigen::JacobiSVD<MatrixXcr>(m).singularValues()(0); }
//template <typename Mat> Real largestSingularValue(const Mat& m) { return Eigen::JacobiSVD<Mat>(m).singularValues()(0); }
// implementing matlab eig
#include <Eigen/Eigenvalues>
VectorXcr eigenValues(const MatrixXcr& m)
{
	Eigen::ComplexEigenSolver<MatrixXcr> ces;
	ces.compute(m, /* computeEigenvectors = */ false);
	return ces.eigenvalues();
}

//////////////////////////////////////////////////////////////////////////////////////
// The parameters and helper constants of SemiGlobal algorithm by Schaefer2017      //
//////////////////////////////////////////////////////////////////////////////////////
SemiGlobalParams::SemiGlobalParams(
        const OptVecXcr& ev_dom,     // The boundaries of the eigenvalue domain of G, when a Chebyshev algorithm is used
        const VectorXcr& ui,         // the initial state vector
        const Real&      dt_,        // timestep
        const int&       Nt_ts,      // the number of interior Chebyshev time points in each time-step (M in the article).
        const int&       Nfm,        // the number of expansion terms in computation of function of matrix (K in the article)
        const Real&      tol_,       // desired error tolerance
        const int&       Gdiff_mvec, // the number of matrix-vector multiplications used by computation of operation of Gdiff_op
        const int&       niterWarn_  // after how many sub-timesteps in the self convergence loop it will print warnings
        )
        : display_mode { true }
        , Niter { 10 }                                           // max internal iterations
        , Niter1st { 16 }                                        // on first execution allow more internal iterations because there is no extrapolated wavefunction
        , niterWarn { niterWarn_ }                               // after how many sub-timesteps in the self convergence loop it will print warnings
        , M_Nt_ts { Nt_ts }                                      // the number of interior Chebyshev time points in each time-step (M in the article)
        , K_Nfm { Nfm }                                          // the number of expansion terms in computation of function of matrix (K in the article)
        , Nu { ui.size() }                                       // amount of grid points in the wavefunction, positional representation
        , tol { tol_ }                                           // desired error tolerance
        , Gdiff_matvecs { Gdiff_mvec }                           // the number of matrix-vector multiplications used by computation of operation of Gdiff_op
        , Arnoldi { not ev_dom }                                 // use Arnoldi when no energy range is provided
        , ev_domain { ev_dom.value_or(VectorXcr {}) }            // The boundaries of the eigenvalue domain of G, when a Chebyshev algorithm is used
        , dt { dt_ }                                             // timestep
        , tmidi { static_cast<int>(std::round(Nt_ts / 2.)) - 1 } // The index of the middle term in the time step // Calculates s_ext with respect to this.
        , t_ts { getEq117(Nt_ts, dt) }                           // The Chebyshev points for expansion in time, in the domain of the time variable
        , test_tpoint { (t_ts(tmidi) + t_ts(tmidi + 1)) / 2 }    // Test time point for error estimation
        , t_ts_times_4 { VectorXr((VectorXr(Nt_ts + 1) << t_ts, test_tpoint).finished() * (4 / dt)) }
        // ↓ time points: -cos(…), test time point, future time step -cos(…), future test time point // https://eigen.tuxfamily.org/dox/structEigen_1_1CommaInitializer.html
        // ↓ in octave it was: t_2ts = [t_ts, test_tpoint, dt + t_ts(2:Nt_ts), dt + test_tpoint];
        // ↓ TODO: in libeigen 3.4 it should be possible to directly initialize with: t_2ts({t_ts, test_tpoint, (dt + t_ts.segment(1, Nt_ts - 1).array()), (dt + test_tpoint)});
        , t_2ts { (VectorXr(Nt_ts * 2 + 1) << t_ts, test_tpoint, (dt + t_ts.segment(1, Nt_ts - 1).array()), (dt + test_tpoint)).finished() }
        , timeMcomp { maketM(t_2ts.segment(1, 2 * Nt_ts), Nt_ts) }         // % Computing the matrix of the time Taylor polynomials.
        , timeMts { timeMcomp.block(0, 0, timeMcomp.rows(), Nt_ts) }       // % timeMts contains the points in the current time step, and timeMnext
        , timeMnext { timeMcomp.block(0, Nt_ts, timeMcomp.rows(), Nt_ts) } // % contains the points in the next time step:
        // % Computing the coefficients of the transformation from the Newton interpolation polynomial terms, to a Taylor like form:
        , Cr2t { r2Taylor4(t_ts, dt) }
        , s_ext_i { getSExtI(Nt_ts, tmidi) }
{
	if (niterWarn >= std::min(Niter, Niter1st)) {
		std::cerr << "Error: --niterWarn >= std::min(Niter, Niter1st). Not enough internal iterations allowed.\n";
		exit(1);
	}
}

VectorXr SemiGlobalParams::getEq117(const int& Nt_ts, const Real& Tts)
{
	// In libeigen 3.4 we could use https://eigen.tuxfamily.org/index.php?title=3.4#New_Major_Features_in_Core
	// #include <Core/ArithmeticSequence.h> and then tcheb = -(helper_count_0_Nt_ts.array() * Mathr::PI / (Nt_ts - 1)).cos(); see git show 1d54df4ad
	VectorXr tcheb(Nt_ts);
	for (int i = 0; i < Nt_ts; i++)
		tcheb(i) = -cos(Real(i) * Mathr::PI / (Nt_ts - 1)); // Schaefer2017 Eq.(117)
	return Real(0.5) * (tcheb.array() + 1) * Tts;               // t_ts = 0.5*(tcheb + 1)*Tts;
}

VectorXi SemiGlobalParams::getSExtI(const int& Nt_ts, const int& tmidi)
{
	// % The indices for the computation of s_ext (excluding tmidi) // Notka: bez tmidi, ponieważ to względem niego jest liczone. Czyli w tamtym punkcie musi być zero.
	VectorXi s_ext_i = VectorXi::Zero(Nt_ts);
	for (int i = 0; i < Nt_ts; i++) {
		s_ext_i(i) = (i < tmidi) ? i : i + 1;
	}
	return s_ext_i;
}

// these are static, because they are called from the class constructor
MatrixXr SemiGlobalParams::maketM(const VectorXr& t, const int& Nt_ts)
// % Computation of the matrix timeM of time Taylor polynomials.
// % timeM(i, j) = t(j)^(i - 1)
{
	const int Nt    = t.size();
	MatrixXr  timeM = MatrixXr::Zero(Nt_ts, Nt);
	timeM.row(0).array() += 1;
	for (int vi = 1; vi < Nt_ts; vi++) {
		timeM.row(vi) = t.transpose().array() * timeM.row(vi - 1).array();
	}
	return timeM;
}

MatrixXr SemiGlobalParams::r2Taylor4(const VectorXr& x, const Real& Dsize)
{
	// % The coefficients contain the 1/n! factor.
	// % The domain is of size 4 (capacity 1).
	// % Dsize: size of the domain.
	const Real Dfactor = 4 / Dsize;
	const int  NC      = x.size();
	MatrixXr   Q       = MatrixXr::Zero(NC, NC);
	// % First row:
	Q(0, 0) = 1;
	// % All the rest of the rows:
	for (int ri = 1; ri < NC; ri++) {
		Q(ri, 0) = -Dfactor * x(ri - 1) * Q(ri - 1, 0);
		for (int Taylori = 1; Taylori < ri; Taylori++) {
			Q(ri, Taylori) = (Q(ri - 1, Taylori - 1) - x(ri - 1) * Q(ri - 1, Taylori)) * Dfactor;
		}
		Q(ri, ri) = Q(ri - 1, ri - 1) * Dfactor;
	}
	return Q;
}

//////////////////////////////////////////////////////////////////////////////////////
// A helper class used for estimation of numerical errors during calculation.       //
//////////////////////////////////////////////////////////////////////////////////////
EstimatedErrors::EstimatedErrors(const Real& arg, Real errConv)
        : errM_Nt_ts_subTimeSteps { arg }
        , errK_Nfm_funcOfMatrix { arg }
        , errFuncOfMatrix { arg }
        , errConvergence { errConv }
{
}

void EstimatedErrors::updateMaxErrors(const EstimatedErrors& other)
{
	using std::max;
	errM_Nt_ts_subTimeSteps = std::max(errM_Nt_ts_subTimeSteps, other.errM_Nt_ts_subTimeSteps);
	errK_Nfm_funcOfMatrix   = std::max(errK_Nfm_funcOfMatrix, other.errK_Nfm_funcOfMatrix);
	errFuncOfMatrix         = std::max(errFuncOfMatrix, other.errFuncOfMatrix);
	errConvergence          = std::max(errConvergence, other.errConvergence);
}

void EstimatedErrors::print()
{
	std::cout << std::setprecision(15) << std::scientific << " f_error =  " << errFuncOfMatrix << "\n texp = " << errM_Nt_ts_subTimeSteps
	          << "\n fU = " << errK_Nfm_funcOfMatrix << "\n conv = " << errConvergence << "\n\n";
}

void EstimatedErrors::checkStart(const ErrorMessageData& er)
{
	if (not er.cpar.Arnoldi) checkFuncOfMatrix(er);
}
void EstimatedErrors::checkInsideSubTimestepLoop(const ErrorMessageData& er)
{
	// in fact display_mode could be an int, e.g. to tell after how many iterations it prints something.
	if (not er.cpar.display_mode) return;
	checkM_Nt_ts_subTimeSteps(er);
	checkK_Nfm_funcOfMatrix(er);
	if (er.cpar.Arnoldi) checkFuncOfMatrix({ er.cpar, 1e-5, er.tsi, er.niter, er.ode });
}
void EstimatedErrors::checkAll(const ErrorMessageData& er)
{
	checkInsideSubTimestepLoop(er);
	checkConvergence(er);
}

void EstimatedErrors::checkM_Nt_ts_subTimeSteps(const ErrorMessageData& er)
{ // fprintf('\nWarning: The maximal estimated error of the time expansion (%d) is larger than the requested tolerance.\nThe solution might be inaccurate.\n', max_texp_error)
	if (errM_Nt_ts_subTimeSteps > er.tol and er.niter >= er.cpar.niterWarn) {
		// Czasem można zmniejszyć matvecs zwiększając Nfm oraz dt.
		std::cerr << "Warning: --Nt_ts,-M error=" << errM_Nt_ts_subTimeSteps << " at substep " << er.niter << ", time expansion error in U";
		std::cerr << " is larger than --tol=" << er.tol << ". Find better -M=" << er.cpar.M_Nt_ts << " or smaller dt;";
		std::cerr << " minimize matvecs/iter: " << Real(er.ode.getMatvecs()) / er.tsi << ", iter= " << er.tsi << "\n";
		// The solution might be inaccurate
	}
}

void EstimatedErrors::checkK_Nfm_funcOfMatrix(const ErrorMessageData& er)
{ // fprintf('\nWarning: The maximal estimated error resulting from the function of matrix computation (%d) is larger than the requested tolerance.\nThe solution might be inaccurate.\n', max_fUerror)
	if (errK_Nfm_funcOfMatrix > er.tol and er.niter >= er.cpar.niterWarn) {
		std::cerr << "Warning: --Nfm,-K error=" << errK_Nfm_funcOfMatrix << " at substep " << er.niter << ", function of matrix error in U";
		std::cerr << " is larger than --tol=" << er.tol << ". Find better -K=" << er.cpar.K_Nfm << " or smaller dt;";
		std::cerr << " minimize matvecs/iter: " << Real(er.ode.getMatvecs()) / er.tsi << ", iter= " << er.tsi << "\n";
		// The solution might be inaccurate
	}
}

void EstimatedErrors::checkFuncOfMatrix(const ErrorMessageData& er)
{ // fprintf('\nWarning: The maximal estimated error of the function of matrix (%d) is larger than 1e-5.\nInstability in the propagation process is possible.\n', max_f_error)
	if (errFuncOfMatrix > er.tol) {
		std::cerr << "Warning: errFuncOfMatrix = " << errFuncOfMatrix << ", estimated error of the function of matrix is larger than " << er.tol;
		std::cerr << ". Instability may occur (in time step No. " << er.tsi << ").\n";
	}
}

void EstimatedErrors::checkConvergence(const ErrorMessageData& er)
{ // fprintf('\nWarning: The maximal estimated error resulting from the iterative process (%d) is larger than the requested tolerance.\nThe solution might be inaccurate.\n', max_conv_error)
	if (errConvergence > er.tol) {
		std::cerr << "Warning: errConvergence = " << errConvergence << ". The maximal estimated error resulting from the iterative process\n";
		std::cerr << "is larger than the requested tolerance. The solution might be inaccurate (in time step No. " << er.tsi << ").\n";
	}
}

void EstimatedErrors::checkNiterIsEnough(const ErrorMessageData& er)
{
	// In fact display_mode could be an int, so that for example decide after how many iterations it starts complaining.
	if (not er.cpar.display_mode) return;
	if ((er.niter == (er.tsi == 0 ? er.cpar.Niter1st : er.cpar.Niter))) {
		std::cerr << "Warning: The program has failed to achieve the desired tolerance in the time sub-iterations of Chebyshev time steps. ";
		std::cerr << "Change --Nt_ts,-M=" << er.cpar.M_Nt_ts << " and/or --Nfm,-K=" << er.cpar.K_Nfm << " (iter=" << er.tsi << ").\n";
	}
}

//////////////////////////////////////////////////////////////////////////////////////
// A helper debug structure which contains the history of propagation               //
//////////////////////////////////////////////////////////////////////////////////////
History::History(const SemiGlobalParams& cpar, const VectorXcr& ui, const int& steps)
{
	Arnoldi  = cpar.Arnoldi;
	Arnoldi  = cpar.Arnoldi;
	U        = MatrixXcr::Zero(cpar.Nu, steps * (cpar.M_Nt_ts - 1) + 1); // It uses lots of memory, so by default it is not used. See how test_source_term.cpp calls append(…)
	U.col(0) = ui;
	t        = VectorXr::Zero(U.cols());
	for (int i = 0; i < t.size(); i++) {
		t(i) = cpar.t_ts(i % (cpar.M_Nt_ts - 1));
		t(i) += (i / (cpar.M_Nt_ts - 1)) * cpar.dt + 0; // steps: + cpar.tinit;
	}
	texp_error = VectorXr::Zero(steps);
	if (cpar.Arnoldi) { f_error = VectorXr::Zero(steps); }
	fUerror    = VectorXr::Zero(steps);
	conv_error = VectorXr::Zero(steps);
	niter      = VectorXr::Zero(steps);

	saved_Nt_ts = cpar.M_Nt_ts;
}
void History::print()
{
	MatrixXr r(niter.rows(), Arnoldi ? 5 : 4);
	r.col(0) = texp_error.col(0);
	r.col(1) = fUerror.col(0);
	r.col(2) = conv_error.col(0);
	r.col(3) = niter.col(0);
	if (Arnoldi) { r.col(4) = f_error.col(0); }
	std::cout << std::setprecision(15) << std::scientific << r << "\n\n";
}
void History::append(const int& tsi, const int& niter_, const EstimatedErrors& currentErrors, const boost::optional<const MatrixXcr> UnewInterTimesteps)
{
	if (UnewInterTimesteps) { U.block(0, (tsi * (saved_Nt_ts - 1) + 1), U.rows(), saved_Nt_ts - 1) = UnewInterTimesteps.get(); }

	texp_error(tsi) = currentErrors.errM_Nt_ts_subTimeSteps;
	if (Arnoldi) { f_error(tsi) = currentErrors.errFuncOfMatrix; }
	fUerror(tsi)    = currentErrors.errK_Nfm_funcOfMatrix;
	conv_error(tsi) = currentErrors.errConvergence;
	niter(tsi)      = niter_;
}

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations                                                             //
//////////////////////////////////////////////////////////////////////////////////////
MatrixXcr f_fun(const VectorXcr& z /* energy domain, spectrum */, const VectorXr& t, const int& Nt_ts, const Real& tol);
//////////////////////////////////////////////////////////////////////////////////////
// The parameters and helper constants when using the Chebyshev method              //
//////////////////////////////////////////////////////////////////////////////////////
CchebData::CchebData(const SemiGlobalParams& cpar, EstimatedErrors& maxErrors)
{
	const VectorXr t_segment = cpar.t_2ts.segment(1, cpar.t_2ts.size() - 1);
	const int&     Nt_ts     = cpar.M_Nt_ts;
	const int&     Nfm       = cpar.K_Nfm;
	const int&     Nu        = cpar.Nu;
	const Real&    tol       = cpar.tol;
	const Real&    Tts       = cpar.dt;

	min_ev = cpar.ev_domain(0);
	max_ev = cpar.ev_domain(1);
	// % Computing the coefficients for the Chebyshev expansion of the
	// % function of matrix, in all the interior time points.
	// % CchebFts contains the coefficients of the current time step, and
	// % CchebFnext contains the coefficients of the next one.
	// Ccheb_f_comp = f_chebC(t_2ts(2:(2*Nt_ts + 1)))
	const MatrixXcr Ccheb_f_comp = f_chebC(t_segment, Nfm, min_ev, max_ev, tol, Nt_ts);
	Ccheb_f_ts                   = Ccheb_f_comp.block(0, 0, Ccheb_f_comp.rows(), Nt_ts);
	Ccheb_f_next                 = Ccheb_f_comp.block(0, Nt_ts, Ccheb_f_comp.rows(), Nt_ts);

	// % Computing evenly spaced sampling points for the error test of the
	// % function of matrix error:
	const Complex dz_test = (max_ev - min_ev) / Real(Nu + 1);
	VectorXcr     helper_count_1_Nu(Nu);
	for (int i = 0; i < Nu; i++) {
		helper_count_1_Nu(i) = i + 1;
	}
	const VectorXcr ztest = min_ev + dz_test * helper_count_1_Nu.array();
	VectorXr        helper_Tts(1);
	helper_Tts(0)                  = Tts;
	const MatrixXcr fztest         = f_fun(ztest, helper_Tts, Nt_ts, tol);
	const VectorXcr sub_Ccheb_f_ts = Ccheb_f_ts.col(Nt_ts - 2);
	maxErrors.errFuncOfMatrix      = ((chebc2result(sub_Ccheb_f_ts, cpar.ev_domain, ztest) - fztest).array().abs() / fztest.array().abs()).maxCoeff();
};

boost::optional<const CchebData> CchebData::maybeMakeCchebData(const SemiGlobalParams& cpar, EstimatedErrors& maxErrors)
{
	return cpar.Arnoldi ? boost::optional<const CchebData>(boost::none) : CchebData(cpar, maxErrors);
}
// these are static, because they are called from the class constructor
MatrixXcr CchebData::chebcM(const MatrixXcr& fM)
{
	// Schaefer2017 być może Eq(139) lub Eq.(146)
	const int  N    = fM.rows();
	const int  M    = fM.cols();
	const Real invN = Real(1) / Real(N);
	MatrixXcr  C    = MatrixXcr::Zero(N, M);
	// TODO: maybe call fftw, see FFT.hpp and https://www.fftw.org/fftw3_doc/index.html , but this function is executed only once, when preparing data.
	//doDCT_2d(fM, C);
	for (int m = 0; m < M; m++) {                 // each column treated separately
		for (int k = 0; k < N; k++) {         // I am calculating the k-th element in column m
			for (int n = 0; n < N; n++) { // for each 'k' there's a summand coming from each 'n'
				using std::cos;
				C(k, m) += invN * Real((k == 0) ? 1 : 2) * fM(n, m) * cos(Mathr::PI * Real(2 * n + 1) * k / Real(2 * N));
			}
		}
	}
	return C;
}

MatrixXcr CchebData::f_chebC(const VectorXr& t, const int& Nfm, const Complex& min_ev, const Complex& max_ev, const Real& tol, const int& Nt_ts)
{
	// this is somewhere around equation Schaefer2017 Eq.(87) (43)
	VectorXr helper_Nfm_seq(Nfm);
	for (int i = 0; i < Nfm; i++) {
		helper_Nfm_seq(i) = 2 * i + 1;
	}
	const VectorXr  zsamp_cheb = (helper_Nfm_seq.array() * Mathr::PI / (2 * Nfm)).cos();
	const VectorXcr zsamp      = Real(0.5) * (zsamp_cheb.array() * (max_ev - min_ev) + max_ev + min_ev);
	const MatrixXcr f_zt       = f_fun(zsamp, t, Nt_ts, tol);
	const MatrixXcr Ccheb_f    = chebcM(f_zt); // Schaefer2017 see Eq(139) and Eq.(146)
	return Ccheb_f;
}

VectorXcr CchebData::chebc2result(const VectorXcr& Ccheb, const VectorXcr& xdomain, const VectorXcr& xresult)
{
	const VectorXcr xrcheb = (2 * xresult.array() - xdomain(0) - xdomain(1)) / (xdomain(1) - xdomain(0));
	const int       m      = Ccheb.rows();
	VectorXcr       t1     = VectorXcr::Constant(xresult.rows(), 1);
	VectorXcr       t2     = xrcheb;
	VectorXcr       result = Ccheb(0) * t1 + Ccheb(1) * t2;
	for (int k = 2; k < m; k++) {
		VectorXcr tk = Real(2) * xrcheb.array() * t2.array() - t1.array();
		result       = result + Ccheb(k) * tk;
		t1           = t2;
		t2           = tk;
	}
	return result;
}

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations ← functions which don't have a class yet                    //
//////////////////////////////////////////////////////////////////////////////////////
MatrixXcr f_fun(const VectorXcr& z /* energy domain, spectrum */, const VectorXr& t, const int& Nt_ts, const Real& tol)
{
	int             Nt = t.size(); // amount: the number of sub-timesteps t, for which we want to have coefficients
	int             Nz = z.size(); // probably energies for which we want to have coefficients
	const MatrixXcr zt = z * t.transpose();
	// % Condition for estimating if f_fun(z, t) should be computed directly or by
	// % a "tail" of a Taylor expansion:
	const Real fac           = boost::math::factorial<Real>(Nt_ts);
	const Real eps           = std::numeric_limits<Real /* double */>::epsilon();
	const Real fcEps         = fac * eps;
	MatrixXr   helper_is_big = MatrixXr::Constant(Nz, Nt, 1);
	for (int i = 0; i < Nz; i++) {
		for (int j = 0; j < Nt; j++) {
			using std::abs;
			using std::pow;
			helper_is_big(i, j) = ((fcEps / abs(pow(zt(i, j), Nt_ts))) < tol) ? 1 : 0;
		}
	}
	const MatrixXr& is_big = helper_is_big;

	MatrixXcr result = MatrixXr::Constant(Nz, Nt, 1);
	for (int i = 0; i < Nz; i++) {
		for (int j = 0; j < Nt; j++) {
			if (is_big(i, j) == 1) {
				using std::exp;
				result(i, j) = exp(zt(i, j));
				for (int polyi = 1; polyi <= Nt_ts; polyi++) {
					result(i, j) = Real(polyi) * (result(i, j) - Real(1)) / zt(i, j);
				}
			}
		}
	}

	/* not const */ MatrixXr is_not_converged = Real(1) - is_big.array();
	MatrixXcr                term             = is_not_converged;
	int                      polydeg          = 1;

	while (is_not_converged.maxCoeff() == 1) {
		for (int i = 0; i < Nz; i++) {
			for (int j = 0; j < Nt; j++) {
				if (is_not_converged(i, j) == 1) {
					term(i, j)   = zt(i, j) * term(i, j) / (Real(polydeg + Nt_ts));
					result(i, j) = result(i, j) + term(i, j);
					using std::abs;
					is_not_converged(i, j) = (abs(term(i, j)) / abs(result(i, j)) > eps) ? 1 : 0;
				}
			}
		}
		polydeg += 1;
	}
	result = result.array() * ((VectorXr::Constant(Nz, 1) * t.transpose()).array().pow(Nt_ts));

	return result;
}

MatrixXcr guess0(const VectorXcr& ui, int Np)
{
	// % The function returns the zeroth order approximation for the guess of the
	// % first step:
	return ui * (VectorXcr::Constant(Np, 1).transpose());
}

template <typename VectZ> MatrixXcr divdif(const VectZ& z, const MatrixXcr& fz)
// Attention: these are two different functions when VectZ == VectorXcr versus VectorXr. (it's a template function)
// looks like it's just Eq.(112) from Schaefer2017
/*
% The function computes the divided difference coefficients for a Newton interpolation.
% The routine is based on a divided difference table, where each
% coefficient is given by the last term of a new diagonal in the table.
% The program applies also for an interpolated function which returns a vector.
% Input:
% z: A vector which contains the sampling points
% fz: The function values at z. Different sampling points are represented
% by different columns.
% Output:
% polcoef: The coefficients of the Newton basis polynomials for the Newton
% interpolation. The coefficints for the different Newton basis polynomials
% are represented by different columns.
% diagonal: The last diagonal, for continuing the process to higher orders,
% if necessary.
*/
{
	assert(z.size() == fz.cols());
	const int dim      = fz.rows();
	const int Npoints  = fz.cols();
	MatrixXcr polcoef  = MatrixXcr::Zero(dim, Npoints);
	MatrixXcr diagonal = MatrixXcr::Zero(dim, Npoints);
	polcoef.col(0)     = fz.col(0);
	diagonal.col(0)    = fz.col(0);

	// % coefi indexes the Newton interpolation coefficients.
	// % dtermi indexes the terms of the new diagonal in the divided difference
	// % table. dtermi coincides with the index of the sampling point with the lowest
	// % index of the divided difference. For example, the index of f[z(2), z(3), z(4)]
	// % is dtermi = 2.

	for (int coefi = 1; coefi < Npoints; coefi++) {
		diagonal.col(coefi) = fz.col(coefi);
		for (int dtermi = coefi - 1; dtermi >= 0; dtermi--) {
			// % diagonal(:, dtermi) belongs to the previous diagonal.
			// % diagonal(:, dtermi + 1) belongs to the new diagonal.
			diagonal.col(dtermi) = (diagonal.col(dtermi + 1) - diagonal.col(dtermi)).array() / (z(coefi) - z(dtermi));
		}
		polcoef.col(coefi) = diagonal.col(0);
	}
	// if diagonal is useful, then return it.
	//return std::make_pair(polcoef,diagonal);
	return polcoef;
}

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations ← functions which don't have a class yet                    //
// Chebyshev only                                                                   //
//////////////////////////////////////////////////////////////////////////////////////
template <typename Gop_act_>
MatrixXcr vchebMop(Gop_act_ operatorG, const VectorXcr& u0, const Complex& leftb, const Complex& rightb, const int& Ncheb /* Nfm */)
/*
% The function computes the vectors, which can be used for a Chebyshev
% expansion of any function of the operator, that operates on the vector u0.
% The expansion will be: f(operator)u0 = sum(a_i*M(:, i+1)), i = 0, 1, ..., Ncheb - 1
% The coefficients a_i are f dependent, and will be computed by the
% program chebc.
% The program is useful when we want to compute a number of functions
% of the same operator, which operate on the same vector.
% operator: a function handle of the form: @(v). The function will return
% the operation of the operator on the vector v.
% The eigenvalue domain of the operator is: [leftb rightb].
*/
{
	// % Defining the operator in the domain of the Chebyshev polynomials:
	// % [-1 1]
	// chebop = @(v) (2*operator(v) - (leftb + rightb)*v)/(rightb - leftb);
	auto      chebop = [=](const VectorXcr& v) -> VectorXcr { return (2 * operatorG(v) - (leftb + rightb) * v) / (rightb - leftb); };
	const int dim    = u0.size();
	MatrixXcr M      = MatrixXcr::Zero(dim, Ncheb);
	M.col(0)         = u0;
	M.col(1)         = chebop(u0);
	for (int k = 2; k < Ncheb; k++) {
		M.col(k) = 2 * chebop(M.col(k - 1)) - M.col(k - 2);
	}
	return M;
}

// A longer version of this function, doesn't use LocalData to pass arguments, see other function with the same name.
MatrixXcr Ufrom_vCheb(
        const MatrixXr&                   timeM,
        const MatrixXcr&                  Ccheb_f,
        const MatrixXcr&                  Vcheb,
        const Real&                       f_error,
        const MatrixXcr&                  v_vecs,
        boost::optional<EstimatedErrors&> err)
{
	const MatrixXcr fGv = Vcheb * Ccheb_f;
	const MatrixXcr U   = v_vecs.block(0, 0, v_vecs.rows(), v_vecs.cols() - 1) * timeM + fGv;
	if (err) {
		err->errK_Nfm_funcOfMatrix = f_error * largestSingularValue(fGv.block(0, 0, fGv.rows(), fGv.cols() - 1))
		        / largestSingularValue(U.block(0, 0, U.rows(), U.cols() - 1));
	}
	return U;
}

//////////////////////////////////////////////////////////////////////////////////////
// forward declarations ← functions which don't have a class yet                    //
// Arnoldi only                                                                     //
//////////////////////////////////////////////////////////////////////////////////////
template <typename Gop_act_> /* Hessenberg */ MatrixXcr createKrop(Gop_act_ Op, const VectorXcr& v0, const int& Nv /* Nfm */, MatrixXcr& V /* Upsilon */)
/*
% The function creates an orthonormal Krylov basis of dimension Nv+1, from
% a vector v0, and a function handle Op, which represents the operation of
% an operator on a vector. It uses the Arnoldi algorithm.
% The columns of V are the vectors of the Krylov space.
% H is the extended Hessenberg matrix.
*/
{
	const int lenv = v0.rows();
	V              = MatrixXcr::Zero(lenv, Nv + 1);
	MatrixXcr H    = MatrixXcr::Zero(Nv + 1, Nv);
	V.col(0)       = v0 / largestSingularValue(v0);

	for (int vj = 0; vj < Nv; vj++) {
		V.col(vj + 1) = Op(V.col(vj));
		for (int vi = 0; vi <= vj; vi++) {
			H(vi, vj) = V.col(vi).adjoint() * V.col(vj + 1);
			V.col(vj + 1) -= H(vi, vj) * V.col(vi);
		}
		H(vj + 1, vj) = largestSingularValue(V.col(vj + 1));
		V.col(vj + 1) = V.col(vj + 1) / H(vj + 1, vj);
	}

	//return std::make_pair(V, H);
	return H;
}

// Arnoldi only
Real get_capacity(const VectorXcr sp, const Complex& testp)
// % Computation of the capacity of the eigenvalue domain
{ // OK, tested.
	Real capacity = 1;
	Real Nsp      = sp.rows();
	// Matlab used the following line:
	//   sp_comp = sp(sp ~= testp);
	// to create a vector without testp element. Chance that it is there is small.
	for (int i = 0; i < sp.rows(); i++) {
		if (sp(i) == testp) { Nsp -= 1; }
	}
	for (int i = 0; i < sp.rows(); i++) {
		if (sp(i) != testp) {
			using std::abs;
			using std::pow;
			capacity = capacity * pow(abs(testp - sp(i)), (1 / Nsp));
		}
	}
	return capacity;
}

// Arnoldi only
MatrixXcr getRv(const int& Nfm, const MatrixXcr& v_vecs, const int& Nt_ts, const MatrixXcr& Hessenberg, const VectorXcr& samplingp, const Real& capacity)
/*
% Obtaining the R_n(G)v_vecs(:, Nt_ts + 1) represented in the
% Krylov space, where the R_n(z)'s are the Newton basis polynomials,
% with samplingp as the sampling points.
*/
{
	MatrixXcr Rv = MatrixXcr::Zero(Nfm + 1, Nfm + 1);
	Rv(0, 0)     = largestSingularValue(v_vecs.col(Nt_ts));
	for (int spi = 0; spi < Nfm; spi++) {
		Rv.block(0, spi + 1, spi + 2, 1)
		        = (Hessenberg.block(0, 0, spi + 2, spi + 1) * Rv.block(0, spi, spi + 1, 1) - samplingp(spi) * Rv.block(0, spi, spi + 2, 1)) / capacity;
	}
	return Rv;
}

// Arnoldi method
// A longer version of this function, doesn't use LocalData to pass arguments, see other function with the same name.
MatrixXcr Ufrom_vArnoldi(
        const MatrixXr&                   timeM,
        const VectorXcr&                  samplingp,
        const Real&                       capacity,
        const int&                        Nt_ts,
        const Real&                       tol,
        const MatrixXcr&                  RvKr,
        const MatrixXcr&                  Upsilon,
        const int&                        Nfm,
        const MatrixXcr&                  v_vecs,
        boost::optional<EstimatedErrors&> currentErrors)
/*
% The function computes the solution from the v vectors.
% Input:
% timeM: The matrix of the t powers for the required time-points
% Output:
% U: The solution in the required time points
% f_error aka errFuncOfMatrix       : The estimated relative error of the computation of f_fun(G,t(Nt_ts))v_vecs(:, Nkr + 1)
% fUerror aka errK_Nfm_funcOfMatrix : The estimated relative error of U resulting from the computational error of f(G,t(Nt_ts))v_vecs(:, Nkr + 1)
*/
{
	// % The required time-points are given by their first power:
	const VectorXr tp = timeM.block(1, 0, 1, timeM.cols()).transpose();
	// % Computing the divided differences for the Newton expansion of f_fun, for
	// % all time-points:
	const MatrixXcr fdvd_ts = divdif(samplingp / capacity, f_fun(samplingp, tp, Nt_ts, tol).transpose()).transpose();
	// % The computation of the Newton expansion of f_fun(G, tp)v_vecs(:, Nkr + 1)
	// % in the Krylov space, for all tp:
	const MatrixXcr fGv_kr = RvKr.block(0, 0, Nfm + 1, RvKr.cols()) * fdvd_ts;
	// % Computing the solution in all tp:
	const MatrixXcr U = v_vecs.block(0, 0, v_vecs.rows(), v_vecs.cols() - 1) * timeM + Upsilon.block(0, 0, Upsilon.rows(), Nfm + 1) * fGv_kr;

	if (currentErrors) {
		using std::abs;
		// TODO: Error estimation - the selection of correct matrix block: I am 70% sure it is the same as in the matlab code.
		// % The absolute error:
		Real f_error_abs = abs(fdvd_ts(Nfm, Nt_ts - 2)) * largestSingularValue(RvKr.block(0, Nfm, RvKr.rows(), 1));
		// % The relative error:
		currentErrors->errFuncOfMatrix = f_error_abs / largestSingularValue(fGv_kr.block(0, Nt_ts - 2, fGv_kr.rows(), 1));
		// % The relative error of U, resulting from this computation:
		currentErrors->errK_Nfm_funcOfMatrix = f_error_abs / largestSingularValue(U.block(0, Nt_ts - 2, U.rows(), 1));
	}
	return U;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////                  //////////////////////////////////
//////////////////////////////////    ODE Solver    //////////////////////////////////
//////////////////////////////////                  //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// SemiGlobalODE - and ODE solver class for arbitrary inhomogeneous ODE with source //
// forward declarations of helper classes, defined below                            //
struct EstimatedErrors;  // tracking and estimation of numerical error              //
struct SemiGlobalParams; // helper constants and parameters                         //
struct CchebData;        // helper constants and parameters when using Chebyshev    //
//////////////////////////////////////////////////////////////////////////////////////
SemiGlobalODE::SemiGlobalODE(
        GOperatorFunction Gop_,          // function representing the action of Hamiltonian on psi at given time point: -i*H(t)ψ
        OptionalGDiffFunc Gdiff_op_,     // function representing the difference of action of Hamiltonian in two different time points: -i*(H(t1)ψ1 - H(t2)ψ2)
        OptionalInhFunc   ihfun_,        // function handle which returns the inhomogeneous term vector
        const OptVecXcr&  ev_domain,     // The boundaries of the eigenvalue domain of G = -i*H, used by Chebyshev method
        const VectorXcr&  ui,            // the initial state vector
        const Real&       dt,            // timestep
        const int&        Nt_ts,         // the number of interior Chebyshev time points in each time-step (M in the article)
        const int&        Nfm,           // the number of expansion terms for in computation of function of matrix (K in the article)
        const Real&       tol,           // required error tolerance, determines the number of internal computation self-consistency steps.
        const int&        Gdiff_matvecs, // The number of matrix-vector multiplications required for computation of operation of Gdiff_op, large scale
        const int&        niterWarn      // after how many sub-timesteps in the self convergence loop it will print warnings
        )
        : maxErrors(0)
        , cpar(ev_domain, ui, dt, Nt_ts, Nfm, tol, Gdiff_matvecs, niterWarn)
        , cheb { CchebData::maybeMakeCchebData(cpar, maxErrors) }
        , Uguess { guess0(ui, Nt_ts + 1) }
        , sNext(cpar.Nu)
        , allniter { 0 } // also internal state. Used to calculate matvecs.
        , niter0th { 0 } // so that we can determine 'mniter: the mean number of iteration for a time step, excluding the first step'
        , there_is_ih { (ihfun_ != boost::none) } // Note if there is inhomogeneous term in the equation
        , there_is_Gdiff_op { (Gdiff_op_ != boost::none) }
        , Gop { Gop_ }
        , Gdiff_op { Gdiff_op_ }
        , ihfun { ihfun_ }
{
	maxErrors.checkStart({ cpar, tol, -1, -1, *this });
	if (there_is_ih) {
		sNext = ihfun.get()(0); // initialise s
		if (not there_is_Gdiff_op) {
			std::cerr << "Error: inhomogeneous source term provided without Gdiff_op\n";
			exit(1);
		}
	}
}

// This struct holds the variables which don't need to be remembered between iteration calls.
// They are local variables of SemiGlobalODE::propagateByDtSemiGlobal, but put into separate
// struct to make it more readable.
struct LocalData {
	SemiGlobalODE& sg;
	const int&           Nt_ts;
	const int&           iteration;
	MatrixXcr            s_ext;     // The extended "inhomogeneos" vectors
	MatrixXcr            v_vecs;    // v_vecs are defined in recursively, contain information about time dependence of s_ext
	MatrixXcr            Ulast;     // Each column represents an interior time point in the time step:
	MatrixXcr            s;         // data for storing inhomogeneous term.
	VectorXr             t;         // The time of the interior time points within the time-step
	MatrixXcr            Vcheb;     // Chebyshev -- helper variables needed outside the loop
	VectorXcr            samplingp; // Arnoldi   -- the domain sampling points
	MatrixXcr            RvKr;      // Arnoldi   -- the expansion vectors for Newton approximation of f(G,t)v_vecs in Krylov space
	MatrixXcr            Upsilon;   // Arnoldi   -- the projection matrix
	Real                 capacity;  // Arnoldi   -- domain capacity
	MatrixXcr            Cnewton{}; // used by calcVVectors

	LocalData(SemiGlobalODE& sg_, const int& iteration_)
	        : sg(sg_)
	        , Nt_ts { sg.cpar.M_Nt_ts }
	        , iteration { iteration_ }
	        , s_ext { MatrixXcr::Zero(sg.cpar.Nu, Nt_ts + 1) }  // The extended "inhomogeneos" vectors
	        , v_vecs { MatrixXcr::Zero(sg.cpar.Nu, Nt_ts + 1) } // v_vecs are defined in recursively, contain information about time dependence of s_ext
	        , Ulast { MatrixXcr::Zero(sg.cpar.Nu, Nt_ts + 1) }  // Each column represents an interior time point in the time step:
	        , s { MatrixXcr::Zero(sg.cpar.Nu, Nt_ts + 1) }      // data for storing inhomogeneous term.
	        , t { VectorXr(sg.cpar.t_2ts.segment(0, Nt_ts + 1).array() + (Real(iteration) * sg.cpar.dt)) } // interior time points within the time-step
	        , Vcheb {}        // Chebyshev -- helper variables needed outside the loop
	        , samplingp {}    // Arnoldi   -- the domain sampling points
	        , RvKr {}         // Arnoldi   -- the expansion vectors for Newton approximation of f(G,t)v_vecs in Krylov space
	        , Upsilon {}      // Arnoldi   -- the projection matrix
	        , capacity { 0 }  // Arnoldi   -- domain capacity
		, Cnewton {}      // used by calcVVectors
	{
		if (sg.there_is_ih) { // initialise s
			s.col(0) = sg.sNext;
			for (int i = 1; i < s.cols(); i++) {
				s.col(i) = sg.ihfun.get()(t(i));
			}
		}
	}
	inline bool converged(const EstimatedErrors& currentErrors, const Real& tol)
	{
		return not(
		        currentErrors.errConvergence > tol && ((iteration > 0 && sg.niter < sg.cpar.Niter) || (iteration == 0 && sg.niter < sg.cpar.Niter1st)));
	}
	inline void calcSExtended()
	{
		// Calculation of the time inhomogeneous s_ext vectors. Note that s_ext(:, tmidi) is equivalent to s(:, tmidi), and not calculated
		if (sg.there_is_Gdiff_op) {
			for (int i = 0; i < sg.cpar.s_ext_i.size(); i++) {
				s_ext.col(sg.cpar.s_ext_i(i))
				        = sg.Gdiff_op.get()(Ulast.col(sg.cpar.s_ext_i(i)), t(sg.cpar.s_ext_i(i)), Ulast.col(sg.cpar.tmidi), t(sg.cpar.tmidi));
			}
		}
		// If there is an inhomogeneous source term, we add it to the s_ext vectors:
		if (sg.there_is_ih) {
			s_ext.col(sg.cpar.tmidi) = s.col(sg.cpar.tmidi);
			for (int i = 0; i < sg.cpar.s_ext_i.size(); i++) {
				s_ext.col(sg.cpar.s_ext_i(i)) += s.col(sg.cpar.s_ext_i(i));
			}
		}
	}
	inline void calcVVectors()
	{
		// Create Newton interpolation in Taylor like form
		auto& cpar  = sg.cpar;
		// Calculation of the coefficients of the form of Taylor expansion, from the coefficients of the Newton interpolation
		// at the points t_ts. The divided differences are computed by the function divdif. For numerical stability, we have
		// to transform the time points in the time step, to points in an interval of length 4:  // TODO: with eigen 3.4 we can use { … } instead of <<
		// Cnewton is the s_ext (meaning it has the time dependency of H and inhomogeneity) and is expressed as divdif (as a Newton interpolation divided differences)
		Cnewton = divdif(cpar.t_ts_times_4, s_ext);
		// Calculating the Taylor like coefficients
		MatrixXcr Ctaylor = MatrixXcr::Zero(cpar.Nu, Nt_ts);
		Ctaylor.col(0)    = Cnewton.col(0);
		for (int Newtoni = 1; Newtoni < Nt_ts; Newtoni++) {
			for (int j = 0; j <= Newtoni; j++) {
				Ctaylor.col(j) += Cnewton.col(Newtoni) * cpar.Cr2t(Newtoni, j);
			}
		}
		// Calculation of the v_vecs vectors
		for (int polyi = 1; polyi < Nt_ts + 1; polyi++) {
			v_vecs.col(polyi) = (sg.Gop(Ulast.col(cpar.tmidi), t(cpar.tmidi), v_vecs.col(polyi - 1)) + Ctaylor.col(polyi - 1)) / Real(polyi);
		}
	}
	inline void checkAllFinite(const EstimatedErrors& currentErrors)
	{
		// checks allFinite
		if (not v_vecs.col(Nt_ts).allFinite()) { // checks the last column of v_vecs
			std::cout << "\nError: The algorithm diverges in time step No. " << iteration << ". change Nts, Nt_ts (M) and/or Ncheb/Nfm (K).\n";
			sg.maxErrors.updateMaxErrors(currentErrors);
			throw std::runtime_error("Error: The algorithm diverges, change M (Nt_ts) or K (Nfm).");
		}

	}
	inline void estimateErrors(EstimatedErrors& currentErrors)
	{
		auto& cpar      = sg.cpar;
		const Real& tol = cpar.tol;     // tolerance parameter 
		// Error estimation for the time expansion. A Newton interpolation of s_ext in the test time-point:
		VectorXcr s_ext_testpoint = Cnewton.col(Nt_ts - 1);
		for (int ti = Nt_ts - 2; ti >= 0; ti--) {
			s_ext_testpoint = Cnewton.col(ti) + (Real(4) / cpar.dt) * (cpar.test_tpoint - cpar.t_ts(ti)) * s_ext_testpoint;
		}
		// The error estimation of the time-expansion:
		using std::abs;
		currentErrors.errM_Nt_ts_subTimeSteps
		        = largestSingularValue(s_ext_testpoint - s_ext.col(Nt_ts)) * abs(cpar.dt) / largestSingularValue(sg.Unew.col(Nt_ts));
		currentErrors.checkInsideSubTimestepLoop({ cpar, tol, iteration, sg.niter, sg });
		// Convergence check of u:
		currentErrors.errConvergence = largestSingularValue(sg.Unew.col(Nt_ts - 1) - Ulast.col(Nt_ts - 1)) / largestSingularValue(Ulast.col(Nt_ts - 1));
	}
};

// A shorter version of this function, uses LocalData, see other function with the same name.
inline MatrixXcr Ufrom_vArnoldi(
	const MatrixXr&                   timeM,
	const Real&                       tol,
	const int&                        Nfm, // K in the paper
	const LocalData&                  d,
	boost::optional<EstimatedErrors&> currentErrors = boost::none)
{
	return Ufrom_vArnoldi(timeM, d.samplingp, d.capacity, d.Nt_ts, tol, d.RvKr, d.Upsilon, Nfm, d.v_vecs, currentErrors);
}

// A shorter version of this function, uses LocalData, see other function with the same name.
inline MatrixXcr Ufrom_vCheb(
	const MatrixXr&                   timeM,
	const MatrixXcr&                  Ccheb,
	const EstimatedErrors&            maxErrors,
	const LocalData&                  d,
	boost::optional<EstimatedErrors&> currentErrors = boost::none)
{
	return Ufrom_vCheb(timeM, Ccheb, d.Vcheb, maxErrors.errFuncOfMatrix, d.v_vecs, currentErrors);
}

// In the paper there is enopugh space for 100 characters in a line |.........80.......90........100
void SemiGlobalODE::propagateByDtSemiGlobal(const int& iteration)
{
	const int&  Nt_ts = cpar.M_Nt_ts; // number of interior Chebyshev points (M in article)
	const int&  Nfm   = cpar.K_Nfm;   // number of terms for function of matrix (K in article)
	const Real& tol   = cpar.tol;     // tolerance parameter
	LocalData   d(*this, iteration);  // data with local variables e.g. Eq.15 and Eq.19
	// Unew is the calculated wavefunction. Uguess is prepared at the end of iteration
	// and used later. The extra Nt_ts + 1 column is used for estimating errors.
	Unew              = MatrixXcr::Zero(cpar.Nu, Nt_ts + 1);
	// The first guess for the iterative process, for the convergence of the u values.
	// Each column represents an interior time point in the time step:
	d.Ulast           = Uguess; // first guess for Eq.28
	Unew.col(0)       = d.Ulast.col(0);
	d.v_vecs.col(0)   = d.Ulast.col(0); // Eq.18 and 19
	niter             = 0;      // count iterations in Eq.28
	currentErrors     = EstimatedErrors(0, tol + 1); // error tracking

	// main iteration loop in Eq.28
	while (not d.converged(currentErrors, tol)) { // Eq.27
		d.calcSExtended(); // calculate Eq. 24, inhomogeneos term
		d.calcVVectors();  // calculate Eq. 18 and Eq. 19
		d.checkAllFinite(currentErrors); // check for divergence
		// The G_avg operator acting on v
		auto G_avg = [=](const VectorXcr& v) -> VectorXcr {
			return Gop(d.Ulast.col(cpar.tmidi), d.t(cpar.tmidi), v);
		};
		if (cpar.Arnoldi) { // use Arnoldi method
			// Create the orthogonalized Krylov space by the Arnoldi iteration procedure, Sect. 2.6
			const MatrixXcr Hessenberg = createKrop(G_avg, d.v_vecs.col(Nt_ts), Nfm, d.Upsilon);
			const VectorXcr eigval     = eigenValues(Hessenberg.block(0, 0, Nfm, Nfm));
			const Complex   avgp       = eigval.sum() / Real(Nfm);
			// sampled energy spectrum and extra point to estimate error
			d.samplingp.resize(eigval.rows() + 1);
			d.samplingp << eigval, avgp;
			d.capacity = get_capacity(eigval, avgp);
			// Obtain the expansion vectors for Newton approximation of f(G,t)v_vecs in Krylov space
			d.RvKr = getRv(Nfm, d.v_vecs, Nt_ts, Hessenberg, d.samplingp, d.capacity);
			// use Arnoldi to approximate new iteration of Eq.28
			Unew.block(0, 1, Unew.rows(), Unew.cols() - 1)
			    = Ufrom_vArnoldi(cpar.timeMts, tol, Nfm, d, currentErrors);
		} else { // Use a Chebyshev approximation for the function of matrix computation. Vcheb has:
			// Tn(G(Ulast(:,tmidi), t(tmidi)))*v_vecs(: ,Nt_ts + 1),  n = 0, 1, ..., Nfm-1
			// where Tn(z) are the Chebyshev polynomials, n'th vector is the (n+1)'th col of Vcheb.
			d.Vcheb = vchebMop(G_avg, d.v_vecs.col(Nt_ts), cheb->min_ev, cheb->max_ev, Nfm);
			Unew.block(0, 1, Unew.rows(), Unew.cols() - 1)
			    = Ufrom_vCheb(cpar.timeMts, cheb->Ccheb_f_ts, maxErrors, d, currentErrors);
		}
		d.estimateErrors(currentErrors);
		d.Ulast = Unew;
		niter   = niter + 1;
	}
	currentErrors.checkNiterIsEnough({ cpar, tol, iteration, niter, *this }); // Check if converged.
	maxErrors.updateMaxErrors(currentErrors); // update estimated maximum errors
	if (iteration == 0) niter0th = niter;
	allniter   = allniter + niter;
	fiSolution = Unew.col(Nt_ts - 1); // Finally! store the result wavefunction.
	// The new guess is an extrapolation of the solution within the previous time step:
	Uguess.col(0) = Unew.col(Nt_ts - 1); // result at last timestep is the first one in next iter
	if (cpar.Arnoldi) { // Arnoldi method
		Uguess.block(0, 1, Uguess.rows(), Nt_ts) // extrapolate for the next timestep
		        = Ufrom_vArnoldi(cpar.timeMnext, tol, Nfm, d, boost::none);
	} else { // Chebyshev method
		Uguess.block(0, 1, Uguess.rows(), Nt_ts) // extrapolate for the next timestep
		        = Ufrom_vCheb(cpar.timeMnext, cheb->Ccheb_f_next, maxErrors, d, boost::none);
	}
	if (there_is_ih) { sNext = d.s.col(Nt_ts - 1); }
	maxErrors.checkAll({ cpar, tol, iteration, -niter, *this }); // check esitmated errors
}

int SemiGlobalODE::getMatvecs() const
{
	// % matvecs: the number of Hamiltonian operations
	return allniter * (cpar.M_Nt_ts * (1 + cpar.Gdiff_matvecs) + cpar.K_Nfm - (cpar.Arnoldi ? 0 : 1));
}

DebugResults SemiGlobalODE::getDebugResults(const int& iteration)
{
	// mniter: the mean number of iteration for a time step, excluding the first step
	Real mniter = (iteration != 0) ? (Real(allniter - niter0th) / Real(iteration)) : 0;
	return DebugResults { fiSolution, mniter, getMatvecs(), maxErrors, niter, currentErrors };
}

MatrixXcr SemiGlobalODE::getDebugUnewInterTimesteps() //
{
	return Unew.block(0, 1, Unew.rows(), cpar.M_Nt_ts - 1);
}

const SemiGlobalParams& SemiGlobalODE::getSemiGlobalParams() { return cpar; }
