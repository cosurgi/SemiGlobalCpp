#!/usr/bin/python3
# encoding: utf-8
# syntax:python

# This simple script generates results in Section 5.2.3 "Precision and speed tests" in the article.

import mpmath    # use high precision in comparison
mpmath.mp.dps=40 # with 40 decimal places

# Fig_11 in Coker1993 is single crossing, high momentum
Fig_11={}
# Results from global Chebyshev propagator, ↓ file with results of transmission/reflection   ↓ time spent on calculation (more times: more runs performed)
Fig_11['ref double']      = ['./Fig_11_method_0_double.txt'      , ["0:22.70" , "0:22.88", "0:22.89"]]
Fig_11['ref long_double'] = ['./Fig_11_method_0_long_double.txt' , ["11:33.55", "11:56.86"          ]]
Fig_11['ref float128']    = ['./Fig_11_method_0_float128.txt'    , ["4:15:48" , "4:21:07"           ]]
# Results from semi-global method
Fig_11['double (1)']      = ['./Fig_11_method_2_double.txt'      , ["0:12.61" ]]
Fig_11['long_double (1)'] = ['./Fig_11_method_2_long_double.txt' , ["0:33.87" ]]
Fig_11['float128 (1)']    = ['./Fig_11_method_2_float128.txt'    , ["34:32.02", "34:00.95"]]

# Fig_13 in Coker1993 is single crossing, low momentum
Fig_13={}
Fig_13['ref double']      = ['./Fig_13_method_0_double.txt'      , ["2:39.52", "2:43.55", "2:42.86"]]
Fig_13['ref long_double'] = ['./Fig_13_method_0_long_double.txt' , ["1:20:11", "1:21:47"           ]]
Fig_13['ref float128']    = ['./Fig_13_method_0_float128.txt'    , ["30:19:48", "30:02:08"         ]]

Fig_13['double (1)']      = ['./Fig_13_method_2_double.txt'      , ["1:27.93" ]]
Fig_13['long_double (1)'] = ['./Fig_13_method_2_long_double.txt' , ["3:52.35" ]]
Fig_13['float128 (1)']    = ['./Fig_13_method_2_float128.txt'    , ["3:44:20" ]]

# Fig_16 in Coker1993 is dual crossing, high momentum
Fig_16={}
Fig_16['ref double']      = ['./Fig_16_method_0_double.txt'      , ["0:39.79", "0:39.25", "0:39.28"]]
Fig_16['ref long_double'] = ['./Fig_16_method_0_long_double.txt' , ["18:51.24", "19:36.73"         ]]
Fig_16['ref float128']    = ['./Fig_16_method_0_float128.txt'    , ["7:03:50", "7:02:14"           ]]

Fig_16['double (1)']      = ['./Fig_16_method_2_double.txt'      , ["0:19.48" ]]
Fig_16['long_double (1)'] = ['./Fig_16_method_2_long_double.txt' , ["0:49.11" ]]
Fig_16['float128 (1)']    = ['./Fig_16_method_2_float128.txt'    , ["48:37.20", "49:32.28" ]]

# Fig_15 in Coker1993 is dual crossing, low momentum
Fig_15={}
Fig_15['ref double']      = ['./Fig_15_method_0_double.txt'      , ["1:08.42", "1:05.92", "1:05.62"   ]]
Fig_15['ref long_double'] = ['./Fig_15_method_0_long_double.txt' , ["32:09.24", "32:26.53"            ]]
Fig_15['ref float128']    = ['./Fig_15_method_0_float128.txt'    , ["11:32:48", "11:49:17", "11:33:48"]]

Fig_15['double (1)']      = ['./Fig_15_method_2_double.txt'      , ["0:34.77" ]]
Fig_15['long_double (1)'] = ['./Fig_15_method_2_long_double.txt' , ["1:25.89" ]]
Fig_15['float128 (1)']    = ['./Fig_15_method_2_float128.txt'    , ["1:23:17" ]]

ULP_dict={}
ULP_dict['double']      = mpmath.mpf('2.220446049250313e-16')
ULP_dict['long double'] = mpmath.mpf('1.084202172485504434e-19')
ULP_dict['float128']    = mpmath.mpf('1.925929944387235853055977942584927e-34')

# parse the timing results - turn them into seconds
def howManySeconds(a):# argument is a string e.g. "2:42.53" or "5:38:53"
	s=a.split(":")
	sec=0
	if(len(s)==2):
		sec=float(s[0])*60+float(s[1])
	elif(len(s)==3):
		sec=float(s[0])*60*60+float(s[1])*60+float(s[2])
	else:
		raise RuntimeError("Cannot parse string with time.")
	return sec

# calculate the average number of seconds from multiple timing runs
def averageSeconds(Fig_NN_pos):
	pos=Fig_NN_pos[1]
	suma=0
	count=float(len(pos))
	for a in pos:
		suma = suma + howManySeconds(a)
	return suma / count

# print how much semi-global is faster than global Chebyshev propagator
def printHowMuchFaster(Fig_NN):
	print( "double      : ", averageSeconds(Fig_NN['ref double'     ]) / averageSeconds(Fig_NN['double (1)'     ]) )
	print( "long double : ", averageSeconds(Fig_NN['ref long_double']) / averageSeconds(Fig_NN['long_double (1)']) )
	print( "float128    : ", averageSeconds(Fig_NN['ref float128'   ]) / averageSeconds(Fig_NN['float128 (1)'   ]) )

# compare two files, return maximum ULP error
def findErrorUlp(fname1,fname2,ULP):
	tol = mpmath.mpf(1e-10)
	with open(fname1[0]) as f1:
		txt1 = f1.readlines()
	with open(fname2[0]) as f2:
		txt2 = f2.readlines()
	assert (len(txt1) == len(txt2))

	maxErr = 0
	errCount = 0
	line = 0
	for l in zip(txt1, txt2):
		l0 = l[0].split()
		l1 = l[1].split()
		line += 1
		col = 0
		assert (len(l0) == len(l1))
		for s in zip(l0, l1):
			col += 1
			try:
				f0 = mpmath.mpf(s[0])
				f1 = mpmath.mpf(s[1])
				err = abs(f0 - f1)
				maxErr = max(err, maxErr)
				if (err > tol):
					raise RuntimeError(
					        '\033[91m' + "Error " + str(err) + " larger than " + str(tol) + " in line: " + str(line) + ", col:" + str(col) + '\033[0m'
					)
			except Exception as e:
				print("Exception: " + str(e))
				errCount += 1
	
	#print('\033[93m' + "Max error: " + ("%.3g" % maxErr) + '\033[0m')
	#print('\033[93m' + "Max error/ULP: " + ("%.3g" % (maxErr/ULP)) + '\033[0m')
	if (errCount != 0):
		print("Failed: errCount=" + str(errCount))
		sys.exit(1)
	else:
		pass
		#print("Test OK.")
	
	return (maxErr, maxErr/ULP)

# There are actually a couple more results printed in the table than are presented in the paper. The extra results which are found
# below the diagonal are comparisons in reverse direction: between global Chebyshev propagator and semi-global as reference.
# For example long double global Chebyshev propagator against float128 semi-global. And the error is the same as between
# both being lond double.
def printTables(Fig_NN):
	print('Global Chebyshev propagator vs. Global Chebyshev propagator, but with higher precision')
	print('               double  long_dbl float128')
	print('ref double',end='\t')
	print( int( findErrorUlp(Fig_NN['ref double'     ], Fig_NN['ref double'     ], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref double'     ], Fig_NN['ref long_double'], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref double'     ], Fig_NN['ref float128'   ], ULP_dict['double'     ]) [1]) )
	print('ref long_double',end='\t')

	print(' ',end='\t')
	print( int( findErrorUlp(Fig_NN['ref long_double'], Fig_NN['ref long_double'], ULP_dict['long double']) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref long_double'], Fig_NN['ref float128'   ], ULP_dict['long double']) [1]) )
	print('-------------------------------------')

	print('Semi-global vs. semi-global, but with higher precision')
	print('               double  long_dbl float128')
	print('double (1)',end='\t')
	print( int( findErrorUlp(Fig_NN['double (1)'     ], Fig_NN['double (1)'     ], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['double (1)'     ], Fig_NN['long_double (1)'], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['double (1)'     ], Fig_NN['float128 (1)'   ], ULP_dict['double'     ]) [1]) )
	print('long_double (1)',end='\t')

	print(' ',end='\t')
	print( int( findErrorUlp(Fig_NN['long_double (1)'], Fig_NN['long_double (1)'], ULP_dict['long double']) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['long_double (1)'], Fig_NN['float128 (1)'   ], ULP_dict['long double']) [1]) )
	print('-------------------------------------')

	print('Semi-global vs. Global Chebyshev propagator')
	print('               double  long_dbl float128')
	print('double (1)',end='\t')
	print( int( findErrorUlp(Fig_NN['ref double'     ], Fig_NN['double (1)'     ], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref long_double'], Fig_NN['double (1)'     ], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref float128'   ], Fig_NN['double (1)'     ], ULP_dict['double'     ]) [1]) )

	print('long double (1)',end='\t')
	print( int( findErrorUlp(Fig_NN['ref double'     ], Fig_NN['long_double (1)'], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref long_double'], Fig_NN['long_double (1)'], ULP_dict['long double']) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref float128'   ], Fig_NN['long_double (1)'], ULP_dict['long double']) [1]) )

	print('float128 (1)',end='\t')
	print( int( findErrorUlp(Fig_NN['ref double'     ], Fig_NN['float128 (1)'   ], ULP_dict['double'     ]) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref long_double'], Fig_NN['float128 (1)'   ], ULP_dict['long double']) [1]) , end='\t')
	print( int( findErrorUlp(Fig_NN['ref float128'   ], Fig_NN['float128 (1)'   ], ULP_dict['float128'   ]) [1]) )

	print('=====================================')
	print()


print("""Parameters used by semi-global method:
                M     K         ε
double       -M 3  -K 15  --tol 2.220446049250313e-16                  
long double  -M 3  -K 18  --tol 1.084202172485504434e-19               
float128     -M 3  -K 31  --tol 1.925929944387235853055977942584927e-34
""")
print('Fig_11 in Coker1993 -- single crossing, high momentum')
printTables(Fig_11)
print('Fig_13 in Coker1993 -- single crossing, low momentum')
printTables(Fig_13)
print('Fig_16 in Coker1993 -- dual crossing, high momentum')
printTables(Fig_16)
print('Fig_15 in Coker1993 -- dual crossing, low momentum')
printTables(Fig_15)

print('**** Print how much faster is semi-global than global Chebyshev propagator ****')
print('Fig_11')
printHowMuchFaster(Fig_11)
print('Fig_13')
printHowMuchFaster(Fig_13)
print('Fig_16')
printHowMuchFaster(Fig_16)
print('Fig_15')
printHowMuchFaster(Fig_15)

def sDouble(Fig_NN):
	return averageSeconds(Fig_NN['ref double'     ]) / averageSeconds(Fig_NN['double (1)'     ])

def sLDouble(Fig_NN):
	return averageSeconds(Fig_NN['ref long_double']) / averageSeconds(Fig_NN['long_double (1)'])

def sFloat128(Fig_NN):
	return averageSeconds(Fig_NN['ref float128'   ]) / averageSeconds(Fig_NN['float128 (1)'   ])

print('\nAverage')
print( "double      : ", (sDouble  (Fig_11) + sDouble  (Fig_13) + sDouble  (Fig_16) + sDouble  (Fig_15))/4.0 )
print( "long double : ", (sLDouble (Fig_11) + sLDouble (Fig_13) + sLDouble (Fig_16) + sLDouble (Fig_15))/4.0 )
print( "float128    : ", (sFloat128(Fig_11) + sFloat128(Fig_13) + sFloat128(Fig_16) + sFloat128(Fig_15))/4.0 )
