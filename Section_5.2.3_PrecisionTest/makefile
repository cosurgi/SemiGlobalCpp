################################################################################################
### To run / test everything use:      (these are defined at the end of this file)           ###
###    make testAllFast - will test almost everything which takes less than an hour          ###
###    make testAll     - will test everything, this will take about 7 hours.                ###
################################################################################################
.DEFAULT_GOAL := test_single_dual_crossing_Double
# If ccache is not installed then use instead CXX=g++
#CXX=ccache g++
CXX=g++
# use libeigen library
INCLUDE=-I/usr/include/eigen3/ -I../SemiGlobalCpp
# Most of the files are in ../SemiGlobalCpp
SG=../SemiGlobalCpp

# default target binary for test_single_dual_crossing is the double precision version
TARGET_CROSSING=test_single_dual_crossing_double

# libraries to link with: boost and libfftw; quadmath provides float128 precision and is part of g++ package
LIBS=-lboost_system -lboost_thread -lboost_program_options -lpthread -pthread -lfftw3 -lfftw3l -lfftw3q -lfftw3_threads -lfftw3l_threads -lfftw3q_threads -lquadmath
# The g++ compilation options
OPTIONS=-fPIC -Ofast -std=gnu++17 -Werror -Wformat -Wformat-security -Wformat-nonliteral -Wall -Wextra -Wnarrowing -Wreturn-type -Wuninitialized -Wfloat-conversion -Wcast-align -Wdisabled-optimization -Wtrampolines -Wpointer-arith -Wswitch-bool -Wwrite-strings -Wnon-virtual-dtor -Wreturn-local-addr -Wsuggest-override -Wswitch-default -Wno-error=unused-variable -Wno-error=unused-parameter -Wno-error=cpp -fdce -fstack-protector-strong -Wno-error=maybe-uninitialized
# default precision is double, see file Real.hpp - select other precisions with -DSELECT_LONG_DOUBLE or -DSELECT_FLOAT128, they are chosen in targets (test_source_term_long_double: test_source_term_float128: test_source_term_float128_MinTolerance:) below.
SELECT_PREC=-DSELECT_DOUBLE
SELECT_MIN_TOLERANCE=

##########################################################################################################
### 1. make targets to compile test_single_dual_crossing_Precision.cpp at various numerical precisions ###
###    to run the tests run one of these three                                                         ###
###       make test_single_dual_crossing_Double        # takes about 1 minute                          ###
###       make test_single_dual_crossing_Long_double   # takes about 3 minutes                         ###
###       make test_single_dual_crossing_Float128      # takes about an hour                           ###
##########################################################################################################
test_single_dual_crossing: test_single_dual_crossing_Precision.cpp ${SG}/Real.hpp ${SG}/FFT.hpp ${SG}/Constants.hpp ${SG}/SemiGlobal.hpp ${SG}/SemiGlobal.cpp ${SG}/ArraysTables.cpp ${SG}/ArraysTables.hpp ${SG}/MultiVector.cpp ${SG}/MultiVector.hpp ${SG}/Tully1990_Coker1993_Helpers.cpp ${SG}/Tully1990_Coker1993_Helpers.hpp
	echo -n "**************************************\n***  Compiling                     ***\n***  test_single_dual_crossing.cpp ***\n***  Prec="${SELECT_PREC}"          ***\n**************************************\n  "${SELECT_MIN_TOLERANCE}
	$(CXX) $(INCLUDE) -o ${TARGET_CROSSING} test_single_dual_crossing_Precision.cpp ${SG}/SemiGlobal.cpp ${SG}/ArraysTables.cpp ${SG}/MultiVector.cpp ${SG}/Tully1990_Coker1993_Helpers.cpp $(LIBS) $(OPTIONS) -Wno-error=unused-but-set-variable ${SELECT_PREC} ${SELECT_MIN_TOLERANCE}

test_single_dual_crossing_Double: test_single_dual_crossing
	echo "\n -*-*-*- Running single / dual crossing tests for double precision -*-*-*-\n\n"
	./test_single_dual_crossing_double 1 > ./testResults/fig_11_double.txt
	./test_single_dual_crossing_double 2 > ./testResults/fig_13_double.txt
	./test_single_dual_crossing_double 3 > ./testResults/fig_16_double.txt
	./test_single_dual_crossing_double 4 > ./testResults/fig_15_double.txt
	./numDiffSimpleMpmath.py 1e-15 ./testResults/fig_11_double.txt ./referenceResults/Fig_11_method_2_double.txt
	./numDiffSimpleMpmath.py 1e-15 ./testResults/fig_13_double.txt ./referenceResults/Fig_13_method_2_double.txt
	./numDiffSimpleMpmath.py 1e-15 ./testResults/fig_16_double.txt ./referenceResults/Fig_16_method_2_double.txt
	./numDiffSimpleMpmath.py 1e-15 ./testResults/fig_15_double.txt ./referenceResults/Fig_15_method_2_double.txt

test_single_dual_crossing_Long_double:
	${MAKE} SELECT_PREC=-DSELECT_LONG_DOUBLE TARGET_CROSSING=test_single_dual_crossing_long_double test_single_dual_crossing
	echo "\n -*-*-*- Running single / dual crossing tests for long double precision -*-*-*-\n\n"
	./test_single_dual_crossing_long_double 1 > ./testResults/fig_11_long_double.txt
	./test_single_dual_crossing_long_double 2 > ./testResults/fig_13_long_double.txt
	./test_single_dual_crossing_long_double 3 > ./testResults/fig_16_long_double.txt
	./test_single_dual_crossing_long_double 4 > ./testResults/fig_15_long_double.txt
	./numDiffSimpleMpmath.py 1e-18 ./testResults/fig_11_long_double.txt ./referenceResults/Fig_11_method_2_long_double.txt
	./numDiffSimpleMpmath.py 1e-18 ./testResults/fig_13_long_double.txt ./referenceResults/Fig_13_method_2_long_double.txt
	./numDiffSimpleMpmath.py 1e-18 ./testResults/fig_16_long_double.txt ./referenceResults/Fig_16_method_2_long_double.txt
	./numDiffSimpleMpmath.py 1e-18 ./testResults/fig_15_long_double.txt ./referenceResults/Fig_15_method_2_long_double.txt

test_single_dual_crossing_Float128:
	${MAKE} SELECT_PREC=-DSELECT_FLOAT128 TARGET_CROSSING=test_single_dual_crossing_float128 test_single_dual_crossing
	echo "\n -*-*-*- Running single / dual crossing tests for float128 precision -*-*-*-\n\n"
	./test_single_dual_crossing_float128 1 > ./testResults/fig_11_float128.txt
	./test_single_dual_crossing_float128 2 > ./testResults/fig_13_float128.txt
	./test_single_dual_crossing_float128 3 > ./testResults/fig_16_float128.txt
	./test_single_dual_crossing_float128 4 > ./testResults/fig_15_float128.txt
	./numDiffSimpleMpmath.py 1e-33 ./testResults/fig_11_float128.txt ./referenceResults/Fig_11_method_2_float128.txt
	./numDiffSimpleMpmath.py 1e-33 ./testResults/fig_13_float128.txt ./referenceResults/Fig_13_method_2_float128.txt
	./numDiffSimpleMpmath.py 1e-33 ./testResults/fig_16_float128.txt ./referenceResults/Fig_16_method_2_float128.txt
	./numDiffSimpleMpmath.py 1e-33 ./testResults/fig_15_float128.txt ./referenceResults/Fig_15_method_2_float128.txt

################################################################################################
### 2. make clean  -  will remove all compiled binaries                                      ###
################################################################################################
clean:
	rm -f test_single_dual_crossing_double
	rm -f test_single_dual_crossing_float128
	rm -f test_single_dual_crossing_long_double
	rm -f ./testResults/fig_11_double.txt
	rm -f ./testResults/fig_13_double.txt
	rm -f ./testResults/fig_16_double.txt
	rm -f ./testResults/fig_15_double.txt
	rm -f ./testResults/fig_11_long_double.txt
	rm -f ./testResults/fig_13_long_double.txt
	rm -f ./testResults/fig_16_long_double.txt
	rm -f ./testResults/fig_15_long_double.txt
	rm -f ./testResults/fig_11_float128.txt
	rm -f ./testResults/fig_13_float128.txt
	rm -f ./testResults/fig_16_float128.txt
	rm -f ./testResults/fig_15_float128.txt

################################################################################################
### 3. make testAllFast - will test almost everything which takes less than an hour          ###
###    make testAll     - will test everything, this will take about 7 hours.                ###
################################################################################################
testAllFast: test_single_dual_crossing_Double test_single_dual_crossing_Long_double

testAll:     testAllFast test_single_dual_crossing_Float128

