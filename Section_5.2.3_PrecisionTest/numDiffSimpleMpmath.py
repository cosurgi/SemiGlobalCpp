#!/usr/bin/python3
# encoding: utf-8
# syntax:python

# This simple script is used by the unit tests in makefile to compare current calculations with the reference results.

import sys,mpmath
mpmath.mp.dps=50

tol = mpmath.mpf(sys.argv[1])
fname1 = sys.argv[2]
fname2 = sys.argv[3]

ULP=1
if(len(sys.argv)==5):
	ULP=mpmath.mpf(sys.argv[4])

#print("Comparing " + fname1 + " ↔ " + fname2)

with open(fname1) as f1:
	txt1 = f1.readlines()

with open(fname2) as f2:
	txt2 = f2.readlines()

assert (len(txt1) == len(txt2))

maxErr = 0
errCount = 0
line = 0
for l in zip(txt1, txt2):
	l0 = l[0].split()  # new, freshly calculated, data
	l1 = l[1].split()  # reference data for comparison
	line += 1
	col = 0
	assert (len(l0) == len(l1))
	for s in zip(l0, l1):
		col += 1
		try:
			f0 = mpmath.mpf(s[0])
			f1 = mpmath.mpf(s[1])
			err = abs(f0 - f1)
			maxErr = max(err, maxErr)
			if (err > tol):
				raise RuntimeError(
				        '\033[91m' + "Error " + str(err) + " larger than " + str(tol) + " in line: " + str(line) + ", col:" + str(col) + '\033[0m'
				)
		except Exception as e:
			print("Exception: " + str(e))
			errCount += 1

print('\033[93m' + "Max error: " + ("%.3g" % maxErr) + '\033[0m')
print('\033[93m' + "Max error/ULP: " + ("%.3g" % (maxErr/ULP)) + '\033[0m')
if (errCount != 0):
	print("Failed: errCount=" + str(errCount))
	sys.exit(1)
else:
	print("Test OK.")
